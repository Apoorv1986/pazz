from subprocess import Popen, PIPE
import os,time


def setup():
    print "Building Control Plane Component"
    top_dir = os.path.realpath(os.curdir)
    try:
        os.chdir("PazzControlPlane/ant/build/")
        input = Popen(['ant','jar-and-cleanup',''], stdout=PIPE, bufsize=10485760)
        for line in input.stdout:
            print line
    except:
        print "Building Control Plane Component Failed"
        exit(-1)

    try:
        time.sleep(3)
        print "Building Configuration Converter"
        print  "current path is ",top_dir+"/utils/ConfigurationConverter/build/"
        os.chdir(top_dir+"/utils/ConfigurationConverter/build/")
        #print os.path.realpath(os.curdir)
        input = Popen(['ant', 'jar-and-cleanup', ''], stdout=PIPE, bufsize=10485760)
        for line in input.stdout:
            print line
        os.chdir(top_dir)
    except:
        print "Building Control configuration converter Failed"
        exit(-1)
setup()