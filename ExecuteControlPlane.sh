#!/bin/bash

CONFIG_PATH=""
HEADER_SIZE=0
OPTIND=1
while getopts "h:p:" opt; do
     case "$opt" in
         p)  CONFIG_PATH=$OPTARG
         	if [[ ! -e $CONFIG_PATH ]]
         	then
         	echo "$CONFIG_PATH does not exist"
         	exit 1
         	fi
             ;;
         h)  HEADER_SIZE=$((HEADER_SIZE+$OPTARG))
             ;;

         *) echo "$0 -p <CONFIG_PATH> -h <HEADER_SIZE>"
             exit 1
             ;;
     esac
 done
shift $((OPTIND-1))

echo "CONFIG_PATH is $CONFIG_PATH"
echo "HEADER_SIZE is $HEADER_SIZE"
if [[ "$CONFIG_PATH" != "*/" ]]
    then
    CONFIG_PATH="$CONFIG_PATH""/"
fi
echo $CONFIG_PATH

if [ ! -e "$CONFIG_PATH/generatedconfig/boxes/" ]
    then
    echo "Make sure that you've already generated configuration for boxes:"
    echo "You can do it by running setup-config.sh on the host machine"
    exit 1
fi

cd PazzControlPlane/ant/build/

echo "Starting PazzControlPlane Component"
java -jar PazzMain.jar -h $HEADER_SIZE -p $CONFIG_PATH&

cd ../../../ConsistencyTester
pwd

echo "Starting Consistency Tester"
#TODO Consistency Tester must recieve the arguments for output file and config path
python ConsistencyTester5.py
