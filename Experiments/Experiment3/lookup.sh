#!/bin/bash
count=0

if grep -m 1 '00001010000000010000000100000001' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100000010' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100000011' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100000100' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100000101' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100000110' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100000111' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100001000' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100001010' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100001111' consistency_testing_output; then
    (( count++ ))
fi
############################ Fuzz Diff ########################################
if grep -m 1 '00001010000000010000000100010000' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100010001' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100010010' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100010011' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100010100' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100010101' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100010110' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100010111' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100011000' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100011001' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100011010' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100011011' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100011100' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100011101' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100011110' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00001010000000010000000100011111' consistency_testing_output; then
    (( count++ ))
fi
############################ Fuzz U- ########################################
if grep -m 1 '00001000000000010000000100000001' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00010000000000010000000100000001' consistency_testing_output; then
    (( count++ ))
fi

echo "$count"
