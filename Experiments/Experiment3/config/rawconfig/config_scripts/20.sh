#!/bin/bash
export https_proxy=https://:@proxy.routerlab:8080
export http_proxy=http://:@proxy.routerlab:8080
pip install pbr
pip install ryu
tcpreplay --loop=1 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 packet.pcap
#python /vagrant/config_scripts/packet_gen.py
