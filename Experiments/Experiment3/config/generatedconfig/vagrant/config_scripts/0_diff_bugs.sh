#!/bin/bash
#Author = Seifeddine Fathalli
sudo timedatectl set-timezone Europe/Berlin

time=$(date +"%M:%S.%6N")
echo "Diff=$time" >> /vagrant/config_scripts/BuggyRules/addingRules.txt

# Fuzz Difference
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.0/27,actions=push_verify,set_verify_rule:12,set_verify_port=1,output:3

#echo "dumping the flow from s0"
#ovs-ofctl -O OpenFlow11 dump-flows s0

#sleep 8s
