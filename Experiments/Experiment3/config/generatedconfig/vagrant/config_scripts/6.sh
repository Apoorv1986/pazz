#!/bin/sh
OVSDB_DIR="/usr/local/etc/openvswitch/" 
rm -rf $OVSDB_DIR
if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch 
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance" 
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server" 
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl" 
ovs-vsctl --no-wait init 

echo "Deleting s6" 
ovs-vsctl --no-wait del-br s0

echo "Adding s0"
ovs-vsctl --no-wait --may-exist add-br s0 -- set bridge s0 datapath_type=netdev

echo "Adding ports" 

ovs-vsctl --no-wait --may-exist add-port s0 eth1 -- set Interface eth1 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s0 eth2 -- set Interface eth2 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s0 eth3 -- set Interface eth3 ofport_request=4

echo "Starting ovs-vswitch" 
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach & 

ovs-vsctl --no-wait set bridge s0 protocols=OpenFlow11
sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s0 table=0

echo "adding the sample flow on s0"
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.3/32,actions=set_verify_rule:601,set_verify_port=63,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.8/32,actions=set_verify_rule:602,set_verify_port=63,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=10.1.1.4/32,actions=set_verify_rule:603,set_verify_port=64,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=10.1.1.5/32,actions=set_verify_rule:604,set_verify_port=64,output:1

ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.10/32,actions=set_verify_rule:606,set_verify_port=63,output:1


echo "dumping the flow from s0"
ovs-ofctl -O OpenFlow11 dump-flows s0
sleep 8s
