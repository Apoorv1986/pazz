#!/bin/bash
#Author = Apoorv Shukla



echo "Erasing the table0, aka removing the defaut flow with actions=Normal"
 ovs-ofctl -O OpenFlow11 del-flows s0 table=0
 ovs-ofctl -O OpenFlow11 del-flows s1 table=0
 ovs-ofctl -O OpenFlow11 del-flows s2 table=0
 ovs-ofctl -O OpenFlow11 del-flows s4 table=0
 

echo "adding the sample flow on S0" 
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/28,actions=push_verify,set_verify_rule:1,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=9,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/27,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:4

echo "dumping the flow from S0" 
ovs-ofctl  -O OpenFlow11 dump-flows s0
 
echo "adding the sample flow on S1"  
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=11,dl_type=0x0800,nw_dst=10.1.1.1/28,actions=set_verify_rule:101,set_verify_port=11,output:13

echo "dumping the flow from S1"
ovs-ofctl  -O OpenFlow11 dump-flows s1

echo "adding the sample flow on S2"  
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=21,dl_type=0x0800,nw_dst=10.1.1.1/27,actions=set_verify_rule:201,set_verify_port=21,output:23

echo "dumping the flow from S2"
ovs-ofctl  -O OpenFlow11 dump-flows s2

echo "adding the sample flow on S4" 
ovs-ofctl -O OpenFlow11 add-flow s4 priority=10,table=0,in_port=41,dl_type=0x0800,nw_dst=10.1.1.1/28,actions=push_verify,set_verify_rule:401,set_verify_port=41,output:43
ovs-ofctl -O OpenFlow11 add-flow s4 priority=9,table=0,in_port=44,dl_type=0x0800,nw_dst=10.1.1.1/27,actions=push_verify,set_verify_rule:402,set_verify_port=44,output:3

echo "dumping the flow from S4" 
ovs-ofctl  -O OpenFlow11 dump-flows s4

ovs-vsctl -- --id=@sflow create sflow agent=eth3 target=\"10.0.0.22:6343\" header=128 sampling=64 polling=1 -- set bridge s5 sflow=@sflow
