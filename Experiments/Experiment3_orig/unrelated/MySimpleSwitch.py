#!/usr/bin/python
__author__ = 'Apoorv'
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import RemoteController
from mininet.cli import CLI
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
import subprocess
from multiprocessing import Process
import time
"""
Instructions to run the topo:
    1. Go to directory where this fil is.
    2. run: sudo -E python MySimpleSwitch.py

The topo has 4 switches and 2 hosts. 
"""


class MySimpleSwitch(Topo):
    """Simple topology example."""

    def __init__(self, **opts):
        """Create custom topo."""


        # Initialize topology
        # It uses the constructor for the Topo class
        super(MySimpleSwitch, self).__init__(**opts)

        # Add hosts
        h1 = self.addHost('h1')
        #h2 = self.addHost('h2')

        # Adding switches
        s0 = self.addSwitch('s0', dpid="0000000000000000",protocols='OpenFlow11')
        s1 = self.addSwitch('s1', dpid="0000000000000001",protocols='OpenFlow11')
        s2 = self.addSwitch('s2', dpid="0000000000000002",protocols='OpenFlow11')
        s4 = self.addSwitch('s4', dpid="0000000000000004",protocols='OpenFlow11')
        s5 = self.addSwitch('s5', dpid="0000000000000005",protocols='OpenFlow11')

        # Add links
        self.addLink(h1, s0, 1, 1)
        #self.addLink(h2, s4, 1, 43)

        self.addLink(s0, s1, 3, 11)
        self.addLink(s0, s2, 4, 21)
        self.addLink(s1, s4, 13, 41)
        self.addLink(s2, s4, 23, 44)
        self.addLink(s4, s5, 43, 1)

'''def simpleTest():
    "Create and test a simple network"
    topo = SingleSwitchTopo(n=4)
    net = Mininet(topo)
    net.start()
    print "Dumping host connections"
    dumpNodeConnections(net.hosts)
    print "Testing network connectivity"
    net.pingAll()
    net.stop()
'''
def run():
	c = RemoteController('c', '0.0.0.0', 6633)
    net = Mininet(topo=MySimpleSwitch(), host=CPULimitedHost, controller=None)
    net.addController(c)
    net.start()
    CLI(net)
    net.stop()

        # if the script is run directly (sudo custom/optical.py):
if __name__ == '__main__':
    setLogLevel('info')
    print 'Initializing OVS'
    subprocess.call("./init_ovs.sh")
    print 'Starting mininet'
    p = Process(target = run, args=())
    p.start()
    time.sleep(10)
    print 'installing rules'
    subprocess.call("./sample_ovs_config.sh")


