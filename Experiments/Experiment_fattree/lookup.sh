#!/bin/bash
count=0

######################## Production traffic ###################################
if grep -m 1 '10101100000100000000001000100001' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000100010' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000100011' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000100100' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000100101' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000100110' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000100111' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000101000' consistency_testing_output; then
    (( count++ ))
fi

########################### Different rule_id #################################
if grep -m 1 '10101100000100000000001000101001' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000101010' consistency_testing_output; then
    (( count++ ))
fi
############################ Fuzz Diff ########################################
if grep -m 1 '10101100000100000000001000110000'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000110001'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000110010'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000110011'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000110100'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000110101'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000110110'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000110111'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000111000'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000111001'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000111010'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000111011'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000111100'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000111101'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000111110'  consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '10101100000100000000001000111111'  consistency_testing_output; then
    (( count++ ))
fi
############################ Fuzz U- ########################################
if grep -m 1 '00001000000000010000000100000001' consistency_testing_output; then
    (( count++ ))
fi
if grep -m 1 '00010000000000010000000100000001' consistency_testing_output; then
    (( count++ ))
fi

####################Fuzz Calculation time######################################
if grep -m 1 'Fuzz Calculation time:' o_output; then
    (( count++))
fi

echo "$count"
