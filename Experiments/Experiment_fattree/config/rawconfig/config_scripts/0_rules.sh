#!/bin/bash
#Author = Said Jawad Saidi || Seifeddine Fathalli

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s0 table=0


echo "adding the sample flow on s0"
ovs-ofctl -O OpenFlow11 add-flow s0 priority=8,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/28,actions=push_verify,set_verify_rule:1,set_verify_port=1,output:3

echo "dumping the flow from s0"
ovs-ofctl -O OpenFlow11 dump-flows s0
