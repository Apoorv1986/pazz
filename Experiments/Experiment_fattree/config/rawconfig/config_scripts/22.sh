#!/bin/bash
#Author = Said Jawad Saidi
OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting S22"
ovs-vsctl --no-wait del-br s22

echo "Adding S22"
ovs-vsctl --no-wait --may-exist add-br s22 -- set bridge s22 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s22 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s22 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s22 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s22 eth4 -- set Interface eth4 ofport_request=4



echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s22 protocols=OpenFlow11

sleep 10

echo "delelting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s22 table=0

echo "adding the sample flow on S22"
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:2201,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:2202,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2203,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:2204,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:2205,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2206,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2207,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2208,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:2209,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2210,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2211,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:2212,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:2213,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:2214,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:2215,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:2216,set_verify_port=1,output:3

echo "dumping the flow from S22"
ovs-ofctl -O OpenFlow11 dump-flows s22
sleep 8s
