#!/bin/sh
OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR
if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s31"
ovs-vsctl --no-wait del-br s31

echo "Adding s31"
ovs-vsctl --no-wait --may-exist add-br s31 -- set bridge s31 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s31 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s31 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s31 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s31 eth4 -- set Interface eth4 ofport_request=4


echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s31 protocols=OpenFlow11
sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s31 table=0

echo "adding the sample flow on s31"
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:3101,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:3102,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3103,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:3104,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:3105,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3106,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3107,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3108,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:3109,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3110,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3111,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:3112,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:3113,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:3114,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:3115,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:3116,set_verify_port=1,output:1

echo "dumping the flow from s31"
ovs-ofctl -O OpenFlow11 dump-flows s31
sleep 8s
