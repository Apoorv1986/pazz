#!/bin/bash
#Author = Said Jawad Saidi || Seifeddine Fathalli
sudo timedatectl set-timezone Europe/Berlin

echo "Time when buggy rules get added"
date +"%Y-%m-%d %H:%M:%S,%3N" > /vagrant/config_scripts/BuggyRules/addingRules.txt
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=7,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/27,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=6,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/26,actions=push_verify,set_verify_rule:3,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=0.1.1.1/8,actions=push_verify,set_verify_rule:4,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=2.1.1.1/8,actions=push_verify,set_verify_rule:5,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=4.1.1.1/8,actions=push_verify,set_verify_rule:6,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=8.1.1.1/8,actions=push_verify,set_verify_rule:7,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=16.1.1.1/8,actions=push_verify,set_verify_rule:8,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=32.1.1.1/8,actions=push_verify,set_verify_rule:9,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=64.1.1.1/8,actions=push_verify,set_verify_rule:10,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=128.1.1.1/8,actions=push_verify,set_verify_rule:11,set_verify_port=1,output:3

echo "dumping the flow from s0"
ovs-ofctl -O OpenFlow11 dump-flows s0

sleep 8s
