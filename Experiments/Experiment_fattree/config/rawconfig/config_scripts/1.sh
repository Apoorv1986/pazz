#!/bin/sh
OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR
if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting S1"
ovs-vsctl --no-wait del-br s1

echo "Adding S1"
ovs-vsctl --no-wait --may-exist add-br s1 -- set bridge s1 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s1 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s1 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s1 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s1 eth4 -- set Interface eth4 ofport_request=4


echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s1 protocols=OpenFlow11
sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s1 table=0

echo "adding the sample flow on S1"
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:101,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:102,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.0/24,actions=push_verify,set_verify_rule:103,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.0/24,actions=push_verify,set_verify_rule:104,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:105,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.0/24,actions=push_verify,set_verify_rule:106,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.1.0/24,actions=push_verify,set_verify_rule:107,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:108,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.4.0/24,actions=push_verify,set_verify_rule:109,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.1.0/24,actions=push_verify,set_verify_rule:110,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:111,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:112,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:113,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:114,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:115,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s1 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:116,set_verify_port=1,output:3

echo "dumping the flow from S1"
ovs-ofctl -O OpenFlow11 dump-flows s1
sleep 8s
