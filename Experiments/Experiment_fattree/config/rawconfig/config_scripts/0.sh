#!/bin/bash
#Author = Said Jawad Saidi || Seifeddine Fathalli
sudo timedatectl set-timezone Europe/Berlin

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s0"
ovs-vsctl --no-wait del-br s0

echo "Adding s0"
ovs-vsctl --no-wait --may-exist add-br s0 -- set bridge s0 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s0 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s0 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s0 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s0 eth4 -- set Interface eth4 ofport_request=4
ovs-vsctl --no-wait --may-exist add-port s0 eth5 -- set Interface eth5 ofport_request=5



echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s0 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s0 table=0


echo "adding the sample flow on s0"
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.1.0/24,actions=push_verify,set_verify_rule:1,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:3,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.0/24,actions=push_verify,set_verify_rule:4,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:5,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:6,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.0/24,actions=push_verify,set_verify_rule:7,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.1.0/24,actions=push_verify,set_verify_rule:8,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:9,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.4.0/24,actions=push_verify,set_verify_rule:10,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.1.0/24,actions=push_verify,set_verify_rule:11,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:12,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.4.0/24,actions=push_verify,set_verify_rule:13,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=5,dl_type=0x0800,nw_dst=172.16.1.0/24,actions=push_verify,set_verify_rule:14,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=5,dl_type=0x0800,nw_dst=172.16.2.0/24,actions=push_verify,set_verify_rule:15,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=5,dl_type=0x0800,nw_dst=172.16.3.0/24,actions=push_verify,set_verify_rule:16,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:17,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:18,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:19,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=5,dl_type=0x0800,nw_dst=172.16.20.0/24,actions=push_verify,set_verify_rule:20,set_verify_port=1,output:1


echo "dumping the flow from s0"
ovs-ofctl -O OpenFlow11 dump-flows s0

sleep 8s
