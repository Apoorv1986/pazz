#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s1"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s1 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.33/32,actions=set_verify_rule:159,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s1 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.34/32,actions=set_verify_rule:159,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s1 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.35/32,actions=set_verify_rule:158,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s1 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.36/32,actions=set_verify_rule:159,set_verify_port=1,output:2

sleep 8s
