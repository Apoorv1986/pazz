#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s3"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s3 priority=20,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.37/32,actions=set_verify_rule:358,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s3 priority=20,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.48/32,actions=set_verify_rule:359,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s3 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.39/32,actions=set_verify_rule:360,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s3 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.40/32,actions=set_verify_rule:361,set_verify_port=1,output:2

sleep 8s
