#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s2"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s2 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.37/32,actions=set_verify_rule:261,set_verify_port=1,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s2 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.38/32,actions=set_verify_rule:262,set_verify_port=1,output:4

sleep 8s
