#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s23"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.35/32,actions=set_verify_rule:23101,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.36/32,actions=set_verify_rule:23102,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.39/32,actions=set_verify_rule:23103,set_verify_port=2,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.40/32,actions=set_verify_rule:23104,set_verify_port=2,output:1

#Buggy rules for diff of covered areas
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.48/28,actions=set_verify_rule:23105,set_verify_port=1,output:2

#Buggy rules for uncovered area
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=8.0.0.0/8,actions=set_verify_rule:23106,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=16.0.0.0/8,actions=set_verify_rule:23107,set_verify_port=1,output:2
#sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=32.0.0.0/8,actions=set_verify_rule:23108,set_verify_port=1,output:2
#sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=64.0.0.0/8,actions=set_verify_rule:23109,set_verify_port=1,output:2
#sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=128.0.0.0/8,actions=set_verify_rule:23110,set_verify_port=1,output:2
#sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=192.0.0.0/8,actions=set_verify_rule:23111,set_verify_port=1,output:2

sleep 8s
