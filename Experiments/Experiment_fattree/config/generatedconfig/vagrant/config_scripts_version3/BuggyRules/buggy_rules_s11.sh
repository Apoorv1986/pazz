#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s11"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.33/32,actions=set_verify_rule:11101,set_verify_port=3,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.34/32,actions=set_verify_rule:11102,set_verify_port=3,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.35/32,actions=set_verify_rule:11103,set_verify_port=3,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.36/32,actions=set_verify_rule:11104,set_verify_port=3,output:2

#Buggy rules for diff of covered areas
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.48/28,actions=set_verify_rule:11105,set_verify_port=3,output:1


#Buggy rules for uncovered area
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=8.0.0.0/8,actions=set_verify_rule:11106,set_verify_port=3,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=16.0.0.0/8,actions=set_verify_rule:11107,set_verify_port=3,output:1
#sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=32.0.0.0/8,actions=set_verify_rule:11108,set_verify_port=3,output:1
#sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=64.0.0.0/8,actions=set_verify_rule:11109,set_verify_port=3,output:1
#sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=128.0.0.0/8,actions=set_verify_rule:11110,set_verify_port=3,output:1
#sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=192.0.0.0/8,actions=set_verify_rule:11111,set_verify_port=3,output:1


sleep 8s
