#!/bin/bash
#Author = Zsolt Vagi
sudo timedatectl set-timezone Europe/Berlin

echo "Time when buggy rules get added"
date +"%Y-%m-%d %H:%M:%S,%3N" > /home/routerlab/Pazz/Experiments/Experiment_fattree/config/generatedconfig/vagrant/config_scripts/BuggyRules/rules_timestamp.txt

vagrant ssh s0  -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s0.sh' &
sleep 8

vagrant ssh s1  -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s1.sh' &
sleep 8

vagrant ssh s2  -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s2.sh' &
sleep 8

vagrant ssh s3  -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s3.sh' &
sleep 8

vagrant ssh s11 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s11.sh' &
sleep 8

vagrant ssh s12 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s12.sh' &
sleep 8

vagrant ssh s13 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s13.sh' &
sleep 8

vagrant ssh s21 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s21.sh' &
sleep 8

vagrant ssh s22 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s22.sh' &
sleep 8

vagrant ssh s23 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s23.sh' &
sleep 8

vagrant ssh s24 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s24.sh' &

vagrant ssh s31 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s31.sh' &
sleep 8

vagrant ssh s42 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s42.sh' &
sleep 8
