#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s42"
ovs-vsctl --no-wait del-br s42

echo "Adding s42"
ovs-vsctl --no-wait --may-exist add-br s42 -- set bridge s42 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s42 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s42 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s42 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s42 eth4 -- set Interface eth4 ofport_request=4

echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s42 protocols=OpenFlow11

sleep 10

echo "delelting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s42 table=0

echo "adding the sample flow on s42"
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.171/32,actions=push_verify,set_verify_rule:4201,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.171/32,actions=push_verify,set_verify_rule:4202,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.171/32,actions=push_verify,set_verify_rule:4203,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.171/32,actions=push_verify,set_verify_rule:4204,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.172/32,actions=push_verify,set_verify_rule:4205,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.172/32,actions=push_verify,set_verify_rule:4206,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.172/32,actions=push_verify,set_verify_rule:4207,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.172/32,actions=push_verify,set_verify_rule:4208,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.181/32,actions=push_verify,set_verify_rule:4209,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.181/32,actions=push_verify,set_verify_rule:4210,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.181/32,actions=push_verify,set_verify_rule:4211,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.181/32,actions=push_verify,set_verify_rule:4212,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.182/32,actions=push_verify,set_verify_rule:4213,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.182/32,actions=push_verify,set_verify_rule:4214,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.182/32,actions=push_verify,set_verify_rule:4215,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.182/32,actions=push_verify,set_verify_rule:4216,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.128/28,actions=push_verify,set_verify_rule:4217,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.128/28,actions=push_verify,set_verify_rule:4218,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.128/28,actions=push_verify,set_verify_rule:4219,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.128/28,actions=push_verify,set_verify_rule:4220,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.144/32,actions=push_verify,set_verify_rule:4221,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.144/32,actions=push_verify,set_verify_rule:4222,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.144/32,actions=push_verify,set_verify_rule:4223,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.144/32,actions=push_verify,set_verify_rule:4224,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s42 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/25,actions=push_verify,set_verify_rule:4225,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s42 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/25,actions=push_verify,set_verify_rule:4226,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s42 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/25,actions=push_verify,set_verify_rule:4227,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s42 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/25,actions=push_verify,set_verify_rule:4228,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s42 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.128/25,actions=push_verify,set_verify_rule:4229,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.128/25,actions=push_verify,set_verify_rule:4230,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.128/25,actions=push_verify,set_verify_rule:4231,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.128/25,actions=push_verify,set_verify_rule:4232,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:4233,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:4234,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:4235,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:4236,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:4237,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:4238,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:4239,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s42 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:4240,set_verify_port=1,output:2

echo "dumping the flow from s42"
ovs-ofctl -O OpenFlow11 dump-flows s42
sleep 8s
