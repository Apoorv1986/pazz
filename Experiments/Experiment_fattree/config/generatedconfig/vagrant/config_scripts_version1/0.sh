#!/bin/bash
#Author = Said Jawad Saidi || Seifeddine Fathalli || Zsolt Vági
sudo timedatectl set-timezone Europe/Berlin

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s0"
ovs-vsctl --no-wait del-br s0

echo "Adding s0"
ovs-vsctl --no-wait --may-exist add-br s0 -- set bridge s0 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s0 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s0 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s0 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s0 eth4 -- set Interface eth4 ofport_request=4
ovs-vsctl --no-wait --may-exist add-port s0 eth5 -- set Interface eth5 ofport_request=5



echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s0 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s0 table=0


echo "adding the sample flow on s0"
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:1,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:3,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:4,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.112/28,actions=push_verify,set_verify_rule:5,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.112/28,actions=push_verify,set_verify_rule:6,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.112/28,actions=push_verify,set_verify_rule:7,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.112/28,actions=push_verify,set_verify_rule:8,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.128/28,actions=push_verify,set_verify_rule:9,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.128/28,actions=push_verify,set_verify_rule:10,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.128/28,actions=push_verify,set_verify_rule:11,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.128/28,actions=push_verify,set_verify_rule:12,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:13,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:14,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:15,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:16,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.48/28,actions=push_verify,set_verify_rule:17,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.48/28,actions=push_verify,set_verify_rule:18,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.48/28,actions=push_verify,set_verify_rule:19,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.48/28,actions=push_verify,set_verify_rule:20,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.64/27,actions=push_verify,set_verify_rule:21,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.64/27,actions=push_verify,set_verify_rule:22,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.64/27,actions=push_verify,set_verify_rule:23,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.64/27,actions=push_verify,set_verify_rule:24,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.144/28,actions=push_verify,set_verify_rule:25,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.144/28,actions=push_verify,set_verify_rule:26,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.144/28,actions=push_verify,set_verify_rule:27,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.144/28,actions=push_verify,set_verify_rule:28,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.160/27,actions=push_verify,set_verify_rule:29,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.160/27,actions=push_verify,set_verify_rule:30,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.160/27,actions=push_verify,set_verify_rule:31,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.160/27,actions=push_verify,set_verify_rule:32,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:33,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:34,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:35,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:36,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.20/30,actions=push_verify,set_verify_rule:37,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.20/30,actions=push_verify,set_verify_rule:38,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.20/30,actions=push_verify,set_verify_rule:39,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.20/30,actions=push_verify,set_verify_rule:40,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:41,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:42,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:43,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:44,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:45,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:46,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:47,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:48,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.32/29,actions=push_verify,set_verify_rule:49,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.32/29,actions=push_verify,set_verify_rule:50,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.32/29,actions=push_verify,set_verify_rule:51,set_verify_port=1,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.32/29,actions=push_verify,set_verify_rule:52,set_verify_port=1,output:5

echo "dumping the flow from s0"
ovs-ofctl -O OpenFlow11 dump-flows s0

sleep 8s
