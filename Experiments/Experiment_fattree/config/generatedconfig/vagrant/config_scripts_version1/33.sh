#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting S33"
ovs-vsctl --no-wait del-br s33

echo "Adding S33"
ovs-vsctl --no-wait --may-exist add-br s33 -- set bridge s33 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s33 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s33 eth2 -- set Interface eth2 ofport_request=2

echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s33 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s33 table=0

echo "adding the sample flow on S33"
ovs-ofctl -O OpenFlow11 add-flow s33 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/24,actions=push_verify,set_verify_rule:3301,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s33 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/24,actions=push_verify,set_verify_rule:3302,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s33 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/24,actions=push_verify,set_verify_rule:3303,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s33 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/24,actions=push_verify,set_verify_rule:3304,set_verify_port=1,output:2

echo "dumping the flow from S33"
ovs-ofctl -O OpenFlow11 dump-flows s33
sleep 8s
