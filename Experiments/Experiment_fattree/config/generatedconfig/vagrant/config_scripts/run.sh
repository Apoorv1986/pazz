#!/bin/bash

echo "Inserting Bugs"
#sh /home/routerlab/Pazz/Experiments/Experiment_fattree/config/generatedconfig/vagrant/config_scripts/buggy_rules.sh > inserting_buggy_rules_output

echo "starting packet_gen ..."
vagrant ssh gen20 -c 'cd /vagrant/config_scripts; sudo python packet_gen.py' &

sleep 5

echo "tcpreplay prod"
vagrant ssh gen20 -c 'cd /vagrant/config_scripts; sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=100000 --pps-multi=100000 --intf1=eth1 /vagrant/config_scripts/pcaps/prod_for_fattree_v2.pcap' &
sleep 5

echo "tcpreplay fuzz - U-"
vagrant ssh gen20 -c 'cd /vagrant/config_scripts; sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=500 --pps-multi=500 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz_for_fattree_Uminus_v2.pcap' &

sleep 5

echo "tcpreplay fuzz"
vagrant ssh gen20 -c 'cd /vagrant/config_scripts; sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=500 --pps-multi=500 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz_for_fattree_v2.pcap' &


