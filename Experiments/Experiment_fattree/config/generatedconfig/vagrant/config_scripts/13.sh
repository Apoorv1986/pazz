#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s13"
ovs-vsctl --no-wait del-br s13

echo "Adding s13"
ovs-vsctl --no-wait --may-exist add-br s13 -- set bridge s13 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s13 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s13 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s13 eth3 -- set Interface eth3 ofport_request=3

echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s13 protocols=OpenFlow11

sleep 10

echo "delelting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s13 table=0

echo "adding the sample flow on s13"
ovs-ofctl -O OpenFlow11 add-flow s13 priority=15,table=0,in_port=3,nw_dst=192.168.0.30/32,actions=push_verify,set_verify_rule:1301,set_verify_port=133,output:1
ovs-ofctl -O OpenFlow11 add-flow s13 priority=15,table=0,in_port=1,nw_dst=192.168.0.20/32,actions=pop_verify,output:3

sleep 8

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 192.168.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 192.168.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 192.168.0.0 netmask 255.255.255.0 gw 0.0.0.0  dev eth3

echo "Adding Routes"
sudo route add -net 192.168.0.113 netmask 255.255.255.255 dev eth1
sudo route add -net 192.168.0.20 netmask 255.255.255.255 dev eth3
sudo route add -net 192.168.0.0  netmask 255.255.0.0 gw 192.168.0.113 dev eth1


echo "Installing iperf"
sudo dpkg -i /vagrant/config_scripts/pcaps/libiperf0_3.1.3-1_amd64.deb
sudo dpkg -i /vagrant/config_scripts/pcaps/iperf3_3.1.3-1_amd64.deb

