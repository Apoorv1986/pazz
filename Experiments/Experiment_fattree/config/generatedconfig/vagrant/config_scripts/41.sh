#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s41"
ovs-vsctl --no-wait del-br s41

echo "Adding s41"
ovs-vsctl --no-wait --may-exist add-br s41 -- set bridge s41 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s41 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s41 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s41 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s41 eth4 -- set Interface eth4 ofport_request=4

echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s41 protocols=OpenFlow11

sleep 10

echo "delelting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s41 table=0

echo "adding the sample flow on s41"
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=set_verify_rule:4101,set_verify_port=2,output:1
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=set_verify_rule:4102,set_verify_port=3,output:1
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=set_verify_rule:4103,set_verify_port=4,output:1
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=set_verify_rule:4104,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=set_verify_rule:4105,set_verify_port=3,output:2
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=set_verify_rule:4106,set_verify_port=4,output:2
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.31/32,actions=set_verify_rule:4107,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.31/32,actions=set_verify_rule:4108,set_verify_port=2,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.4.31/32,actions=set_verify_rule:4109,set_verify_port=4,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.32/32,actions=set_verify_rule:4110,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.32/32,actions=set_verify_rule:4111,set_verify_port=2,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.4.32/32,actions=set_verify_rule:4112,set_verify_port=4,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.41/32,actions=set_verify_rule:4113,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.41/32,actions=set_verify_rule:4114,set_verify_port=2,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.4.41/32,actions=set_verify_rule:4115,set_verify_port=3,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.42/32,actions=set_verify_rule:4116,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.42/32,actions=set_verify_rule:4117,set_verify_port=2,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.4.42/32,actions=set_verify_rule:4118,set_verify_port=3,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.23/32,actions=set_verify_rule:4119,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.23/32,actions=set_verify_rule:4120,set_verify_port=2,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.4.23/32,actions=set_verify_rule:4121,set_verify_port=4,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.24/32,actions=set_verify_rule:4122,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.24/32,actions=set_verify_rule:4123,set_verify_port=2,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.4.24/32,actions=set_verify_rule:4124,set_verify_port=4,output:3
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.161/32,actions=set_verify_rule:4125,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.161/32,actions=set_verify_rule:4126,set_verify_port=2,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.161/32,actions=set_verify_rule:4127,set_verify_port=3,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.162/32,actions=set_verify_rule:4128,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.162/32,actions=set_verify_rule:4129,set_verify_port=2,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.162/32,actions=set_verify_rule:4130,set_verify_port=3,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=set_verify_rule:4131,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=set_verify_rule:4132,set_verify_port=2,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=set_verify_rule:4133,set_verify_port=3,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=set_verify_rule:4134,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=set_verify_rule:4135,set_verify_port=2,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=set_verify_rule:4136,set_verify_port=3,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=set_verify_rule:4137,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=set_verify_rule:4138,set_verify_port=2,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=set_verify_rule:4139,set_verify_port=3,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=set_verify_rule:4140,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=set_verify_rule:4141,set_verify_port=2,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=set_verify_rule:4142,set_verify_port=3,output:4
ovs-ofctl -O OpenFlow11 add-flow s41 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=set_verify_rule:4143,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s41 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=set_verify_rule:4144,set_verify_port=3,output:2
ovs-ofctl -O OpenFlow11 add-flow s41 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=set_verify_rule:4145,set_verify_port=4,output:2

sleep 8s

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 172.16.4.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 172.16.4.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4

echo "Adding correct routes"
sudo route add -net 172.16.0.0 netmask 255.255.255.248  dev eth1
sudo route add -net 172.16.0.8 netmask 255.255.255.248  dev eth2
sudo route add -net 172.16.4.31 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.4.32 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.4.23 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.4.24 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.4.41 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.4.42 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.0.161 netmask 255.255.255.255 gw 172.16.4.41 dev eth4
sudo route add -net 172.16.0.162 netmask 255.255.255.255 gw 172.16.4.41 dev eth4
sudo route add -net 172.16.0.16 netmask 255.255.255.248 gw 172.16.4.41 dev eth4
sudo route add -net 172.16.0.24 netmask 255.255.255.255 gw 172.16.4.41 dev eth4
sudo route add -net 172.16.0.31 netmask 255.255.255.255 gw 172.16.4.41 dev eth4
sudo route add -net 172.16.0.32 netmask 255.255.255.252 gw 172.16.4.41 dev eth4
sudo route add -net 172.16.0.0 netmask 255.255.0.0 gw 172.16.14  dev eth2
