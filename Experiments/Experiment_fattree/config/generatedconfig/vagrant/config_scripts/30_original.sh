#!/bin/bash
#Author: Apoorv Shukla
sudo timedatectl set-timezone Europe/Berlin

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR
if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
#mkdir -p /usr/local/etc/openvswitch
#cd /home/vagrant/ovs
#ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
#pkill ovsdb-server
#pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
#ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate$
#ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file

echo "initializing ovs-vsctl"
#ovs-vsctl --no-wait init

echo "Deleting s0"
#ovs-vsctl --no-wait del-br s0

echo "Adding s0"
#ovs-vsctl --no-wait --may-exist add-br s0 -- set bridge s0 datapath_type=netdev

echo "Adding ports"
#ovs-vsctl --no-wait --may-exist add-port s0 eth1 -- set Interface eth1 ofport_request=1


echo "Starting ovs-vswitch"
#export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
#ovs-vswitchd unix:$DB_SOCK --pidfile --detach &
#ovs-vsctl --no-wait set bridge s0 protocols=OpenFlow11

#sleep 10

#ovs-ofctl -O OpenFlow11 del-flows s0 table=0

#ovs-vsctl -- --id=@sflow create sflow agent=eth2 target=\"192.168.20.1:6343\" header=128 sampling=1000 polling=1 -- set bridge s0 sflow=@sflow
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,actions=DROP

echo "Adding sample flow"

#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,actions=normal

#sleep 8s

echo "Installing iperf"
sudo dpkg -i /vagrant/config_scripts/pcaps/libiperf0_3.1.3-1_amd64.deb
sudo dpkg -i /vagrant/config_scripts/pcaps/iperf3_3.1.3-1_amd64.deb

sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 dev eth1
sudo route add -net 172.16.0.6 netmask 255.255.255.255 dev eth1
sudo route add -net 172.16.0.0 netmask 255.255.255.0 gw 172.16.0.6 dev eth1

#sudo ip addr del 172.16.0.30/24 dev eth1
#sudo ip addr add 172.16.0.30/24 dev s0
#sudo ip link set dev s0 up
