#!/bin/bash
#Author = Zsolt Vagi
sudo timedatectl set-timezone Europe/Berlin

echo "Time when buggy rules get added"
date +"%Y-%m-%d %H:%M:%S,%3N" > /vagrant/config_scripts/BuggyRules/addingRules.txt

echo "Adding buggy rules to s0"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.33/32,actions=set_verify_rule:58,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.34/32,actions=set_verify_rule:59,set_verify_port=1,output:3

#Buggy rules where only the rule_id is different
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=set_verify_rule:98,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=set_verify_rule:99,set_verify_port=1,output:2


#Buggy rules for diff of covered areas
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.48/28,actions=set_verify_rule:60,set_verify_port=1,output:2

#Buggy rules for uncovered area
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=8.0.0.0/8,actions=set_verify_rule:61,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=16.0.0.0/8,actions=set_verify_rule:62,set_verify_port=1,output:2
#sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=32.0.0.0/8,actions=set_verify_rule:63,set_verify_port=1,output:2
#sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=64.0.0.0/8,actions=set_verify_rule:64,set_verify_port=1,output:2
#sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=128.0.0.0/8,actions=set_verify_rule:65,set_verify_port=1,output:2
#sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=192.0.0.0/8,actions=set_verify_rule:66,set_verify_port=1,output:2



sleep 8s
