#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s13"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.33/32,actions=push_verify,set_verify_rule:13101,set_verify_port=133,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.34/32,actions=push_verify,set_verify_rule:13102,set_verify_port=133,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.35/32,actions=push_verify,set_verify_rule:13103,set_verify_port=133,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.36/32,actions=push_verify,set_verify_rule:13104,set_verify_port=133,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.37/32,actions=push_verify,set_verify_rule:13105,set_verify_port=133,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.38/32,actions=push_verify,set_verify_rule:13106,set_verify_port=133,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.39/32,actions=push_verify,set_verify_rule:13107,set_verify_port=133,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.40/32,actions=push_verify,set_verify_rule:13108,set_verify_port=133,output:2

#Buggy rules for diff of covered areas
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.48/28,actions=push_verify,set_verify_rule:13109,set_verify_port=133,output:1


#Buggy rules for uncovered area
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=8.0.0.0/8,actions=push_verify,set_verify_rule:13110,set_verify_port=133,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=16.0.0.0/8,actions=push_verify,set_verify_rule:13111,set_verify_port=133,output:1
#sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=32.0.0.0/8,actions=push_verify,set_verify_rule:13112,set_verify_port=3,output:1
#sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=64.0.0.0/8,actions=push_verify,set_verify_rule:13113,set_verify_port=3,output:1
#sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=128.0.0.0/8,actions=push_verify,set_verify_rule:13114,set_verify_port=3,output:1
#sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=192.0.0.0/8,actions=push_verify,set_verify_rule:13115,set_verify_port=3,output:1

sleep 8s
