#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s21"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.33/32,actions=set_verify_rule:21101,set_verify_port=212,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.34/32,actions=set_verify_rule:21102,set_verify_port=212,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.35/32,actions=set_verify_rule:21103,set_verify_port=212,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.36/32,actions=set_verify_rule:21104,set_verify_port=212,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.39/32,actions=set_verify_rule:21105,set_verify_port=213,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.40/32,actions=set_verify_rule:21106,set_verify_port=213,output:4

#Buggy rules where only the rule_id is different
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=set_verify_rule:2198,set_verify_port=211,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=set_verify_rule:2199,set_verify_port=211,output:3


#Buggy rules for diff of covered areas
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.48/28,actions=set_verify_rule:21107,set_verify_port=211,output:3

#Buggy rules for uncovered area
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=8.0.0.0/8,actions=set_verify_rule:21108,set_verify_port=211,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=16.0.0.0/8,actions=set_verify_rule:21109,set_verify_port=211,output:3
#sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=32.0.0.0/8,actions=set_verify_rule:21110,set_verify_port=1,output:3
#sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=64.0.0.0/8,actions=set_verify_rule:21111,set_verify_port=1,output:3
#sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=128.0.0.0/8,actions=set_verify_rule:21112,set_verify_port=1,output:3
#sudo ovs-ofctl -O OpenFlow11 add-flow s21 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=192.0.0.0/8,actions=set_verify_rule:21113,set_verify_port=1,output:3


sleep 8s
