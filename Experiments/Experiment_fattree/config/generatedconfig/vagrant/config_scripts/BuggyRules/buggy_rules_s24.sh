#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s24"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.33/32,actions=push_verify,set_verify_rule:24101,set_verify_port=241,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.34/32,actions=push_verify,set_verify_rule:24102,set_verify_port=241,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.35/32,actions=push_verify,set_verify_rule:24103,set_verify_port=242,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.36/32,actions=push_verify,set_verify_rule:24104,set_verify_port=242,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.37/32,actions=push_verify,set_verify_rule:24105,set_verify_port=242,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.38/32,actions=push_verify,set_verify_rule:24106,set_verify_port=242,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.39/32,actions=push_verify,set_verify_rule:24107,set_verify_port=241,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.40/32,actions=push_verify,set_verify_rule:24108,set_verify_port=241,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:24109,set_verify_port=241,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:24110,set_verify_port=241,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:24111,set_verify_port=242,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:24112,set_verify_port=242,output:3

#Buggy rules for diff of covered areas
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.48/28,actions=push_verify,set_verify_rule:24113,set_verify_port=242,output:3

#Buggy rules for uncovered area
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=8.0.0.0/8,actions=push_verify,set_verify_rule:24114,set_verify_port=242,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=16.0.0.0/8,actions=push_verify,set_verify_rule:24115,set_verify_port=242,output:3
#sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=32.0.0.0/8,actions=push_verify,set_verify_rule:24116,set_verify_port=2,output:3
#sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=64.0.0.0/8,actions=push_verify,set_verify_rule:24117,set_verify_port=2,output:3
#sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=128.0.0.0/8,actions=push_verify,set_verify_rule:24118,set_verify_port=2,output:3
#sudo ovs-ofctl -O OpenFlow11 add-flow s24 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=192.0.0.0/8,actions=push_verify,set_verify_rule:2419,set_verify_port=2,output:3


sleep 8s
