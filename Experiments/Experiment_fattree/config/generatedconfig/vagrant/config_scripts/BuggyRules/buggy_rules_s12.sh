#!/bin/bash
#Author = Zsolt Vagi

echo "Adding buggy rules to s12"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s12 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.37/32,actions=set_verify_rule:12101,set_verify_port=123,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s12 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.38/32,actions=set_verify_rule:12102,set_verify_port=123,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s12 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.39/32,actions=set_verify_rule:12103,set_verify_port=123,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s12 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.40/32,actions=set_verify_rule:12104,set_verify_port=123,output:2

sleep 8s
