#!/bin/bash
#Author = Zsolt Vagi


echo "Adding buggy rules to s22"
#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.35/32,actions=set_verify_rule:22101,set_verify_port=223,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.36/32,actions=set_verify_rule:22102,set_verify_port=223,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.37/32,actions=set_verify_rule:22103,set_verify_port=222,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.38/32,actions=set_verify_rule:22104,set_verify_port=222,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.39/32,actions=set_verify_rule:22105,set_verify_port=222,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.40/32,actions=set_verify_rule:22106,set_verify_port=222,output:3

#Buggy rules for diff of covered areas
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.48/28,actions=set_verify_rule:22107,set_verify_port=223,output:4

#Buggy rules for uncovered area
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=8.0.0.0/8,actions=set_verify_rule:22108,set_verify_port=223,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=16.0.0.0/8,actions=set_verify_rule:22109,set_verify_port=223,output:4
#sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=32.0.0.0/8,actions=set_verify_rule:22110,set_verify_port=3,output:4
#sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=64.0.0.0/8,actions=set_verify_rule:22111,set_verify_port=3,output:4
#sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=128.0.0.0/8,actions=set_verify_rule:22112,set_verify_port=3,output:4
#sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=192.0.0.0/8,actions=set_verify_rule:22113,set_verify_port=3,output:4


sleep 8s
