#!/bin/bash
#Author = Said Jawad Saidi || Seifeddine Fathalli || Zsolt Vági
sudo timedatectl set-timezone Europe/Berlin

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s0"
ovs-vsctl --no-wait del-br s0

echo "Adding s0"
ovs-vsctl --no-wait --may-exist add-br s0 -- set bridge s0 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s0 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s0 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s0 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s0 eth4 -- set Interface eth4 ofport_request=4
ovs-vsctl --no-wait --may-exist add-port s0 eth5 -- set Interface eth5 ofport_request=5


echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s0 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s0 table=0

echo "adding the sample flow on s0"
ovs-ofctl -O OpenFlow11 add-flow s0 priority=15,table=0,in_port=1,nw_dst=192.168.0.30/32,actions=pop_verify,output:5
ovs-ofctl -O OpenFlow11 add-flow s0 priority=15,table=0,in_port=5,nw_dst=192.168.0.20/32,actions=push_verify,set_verify_rule:2,set_verify_port=5,output:1

sleep 8s

echo "Removing default routes"
sudo route del -net 192.168.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 192.168.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 192.168.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 192.168.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4
sudo route del -net 192.168.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth5


echo "Adding routes"
sudo route add -net 192.168.0.111 netmask 255.255.255.255 dev eth1
sudo route add -net 192.168.0.30 netmask 255.255.255.255 dev eth5
sudo route add -net 192.168.0.112 netmask 255.255.255.255 gw 192.168.0.111 dev eth1
sudo route add -net 192.168.0.131 netmask 255.255.255.255 gw 192.168.0.111 dev eth1
sudo route add -net 192.168.0.132 netmask 255.255.255.255 gw 192.168.0.111 dev eth1
sudo route add -net 192.168.0.133 netmask 255.255.255.255 gw 192.168.0.111 dev eth1
sudo route add -net 192.168.0.20 netmask 255.255.255.255 gw 192.168.0.111 dev eth1

echo "Installing iperf"
sudo dpkg -i /vagrant/config_scripts/pcaps/libiperf0_3.1.3-1_amd64.deb
sudo dpkg -i /vagrant/config_scripts/pcaps/iperf3_3.1.3-1_amd64.deb
