#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s21"
ovs-vsctl --no-wait del-br s21

echo "Adding s21"
ovs-vsctl --no-wait --may-exist add-br s21 -- set bridge s21 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s21 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s21 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s21 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s21 eth4 -- set Interface eth4 ofport_request=4


echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s21 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s21 table=0


echo "adding the sample flow on s21"
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:2101,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:2102,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:2103,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:2104,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:2105,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:2106,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.31/32,actions=push_verify,set_verify_rule:2107,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.31/32,actions=push_verify,set_verify_rule:2108,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.31/32,actions=push_verify,set_verify_rule:2109,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.32/32,actions=push_verify,set_verify_rule:2110,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.32/32,actions=push_verify,set_verify_rule:2111,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.32/32,actions=push_verify,set_verify_rule:2112,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2113,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2114,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2115,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:2116,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:2117,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:2118,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.23/32,actions=push_verify,set_verify_rule:2119,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.23/32,actions=push_verify,set_verify_rule:2120,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.23/32,actions=push_verify,set_verify_rule:2121,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.24/32,actions=push_verify,set_verify_rule:2122,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.24/32,actions=push_verify,set_verify_rule:2123,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.24/32,actions=push_verify,set_verify_rule:2124,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.221/32,actions=push_verify,set_verify_rule:2125,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.221/32,actions=push_verify,set_verify_rule:2126,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.221/32,actions=push_verify,set_verify_rule:2127,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.222/32,actions=push_verify,set_verify_rule:2128,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.222/32,actions=push_verify,set_verify_rule:2129,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.222/32,actions=push_verify,set_verify_rule:2130,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:2131,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:2132,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:2133,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:2134,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:2135,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:2136,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:2137,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:2138,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:2139,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:2140,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:2141,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:2142,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s21 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2143,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s21 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2144,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s21 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2145,set_verify_port=1,output:1

echo "dumping the flow from s21"
ovs-ofctl -O OpenFlow11 dump-flows s21
sleep 8s

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 172.16.2.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 172.16.2.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4

echo "Adding correct routes"
sudo route add -net 172.16.0.0 netmask 255.255.255.248  dev eth1
sudo route add -net 172.16.0.8 netmask 255.255.255.248  dev eth2
sudo route add -net 172.16.2.31 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.2.32 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.2.23 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.2.24 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.2.41 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.2.42 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.0.221 netmask 255.255.255.255 gw 172.16.2.41 dev eth4
sudo route add -net 172.16.0.222 netmask 255.255.255.255 gw 172.16.2.41 dev eth4
sudo route add -net 172.16.0.16 netmask 255.255.255.248 gw 172.16.2.41 dev eth4
sudo route add -net 172.16.0.24 netmask 255.255.255.255 gw 172.16.2.41 dev eth4
sudo route add -net 172.16.0.31 netmask 255.255.255.255 gw 172.16.2.41 dev eth4
sudo route add -net 172.16.0.32 netmask 255.255.255.252 gw 172.16.2.41 dev eth4
sudo route add -net 172.16.0.0 netmask 255.255.0.0 gw 172.16.3  dev eth1
