#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting S32"
ovs-vsctl --no-wait del-br s32

echo "Adding S32"
ovs-vsctl --no-wait --may-exist add-br s32 -- set bridge s32 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s32 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s32 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s32 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s32 eth4 -- set Interface eth4 ofport_request=4

echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s32 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s32 table=0

echo "adding the sample flow on S32"
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.23/32,actions=push_verify,set_verify_rule:3201,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.23/32,actions=push_verify,set_verify_rule:3202,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.23/32,actions=push_verify,set_verify_rule:3203,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.33/32,actions=push_verify,set_verify_rule:3204,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.33/32,actions=push_verify,set_verify_rule:3205,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.33/32,actions=push_verify,set_verify_rule:3206,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.31/32,actions=push_verify,set_verify_rule:3207,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.31/32,actions=push_verify,set_verify_rule:3208,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.31/32,actions=push_verify,set_verify_rule:3209,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.32/32,actions=push_verify,set_verify_rule:3210,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.32/32,actions=push_verify,set_verify_rule:3211,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.32/32,actions=push_verify,set_verify_rule:3212,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.41/32,actions=push_verify,set_verify_rule:3213,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.41/32,actions=push_verify,set_verify_rule:3214,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.41/32,actions=push_verify,set_verify_rule:3215,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.42/32,actions=push_verify,set_verify_rule:3216,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.42/32,actions=push_verify,set_verify_rule:3217,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.42/32,actions=push_verify,set_verify_rule:3218,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.13/32,actions=push_verify,set_verify_rule:3219,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.13/32,actions=push_verify,set_verify_rule:3220,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.13/32,actions=push_verify,set_verify_rule:3221,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.14/32,actions=push_verify,set_verify_rule:3222,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.14/32,actions=push_verify,set_verify_rule:3223,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.14/32,actions=push_verify,set_verify_rule:3224,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.51/32,actions=push_verify,set_verify_rule:3225,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.51/32,actions=push_verify,set_verify_rule:3226,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.51/32,actions=push_verify,set_verify_rule:3227,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.52/32,actions=push_verify,set_verify_rule:3228,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.52/32,actions=push_verify,set_verify_rule:3229,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.52/32,actions=push_verify,set_verify_rule:3230,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:3231,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:3232,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:3233,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:3234,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:3235,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:3236,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s32 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3237,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s32 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3238,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s32 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3239,set_verify_port=1,output:2

echo "dumping the flow from S32"
ovs-ofctl -O OpenFlow11 dump-flows s32
sleep 8s

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 172.16.3.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 172.16.3.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4

echo "Adding routes"
sudo route add -net 172.16.0.23 netmask 255.255.255.255  dev eth1
sudo route add -net 172.16.0.33 netmask 255.255.255.255  dev eth2
sudo route add -net 172.16.3.31 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.3.32 netmask 255.255.255.252  dev eth3
sudo route add -net 172.16.3.41 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.3.42 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.3.13 netmask 255.255.255.255 gw 172.16.3.32  dev eth3
sudo route add -net 172.16.3.14 netmask 255.255.255.255 gw 172.16.3.32  dev eth3
sudo route add -net 172.16.0.51 netmask 255.255.255.255 gw 172.16.3.42 dev eth4
sudo route add -net 172.16.0.52 netmask 255.255.255.255 gw 172.16.3.42 dev eth4
sudo route add -net 172.16.0.0 netmask 255.255.255.248 gw 172.16.2.32 dev eth3
sudo route add -net 172.16.0.8 netmask 255.255.255.248 gw 172.16.2.32 dev eth3
sudo route add -net 172.16.0.0 netmask 255.255.0.0 gw 172.16.0.33  dev eth2
