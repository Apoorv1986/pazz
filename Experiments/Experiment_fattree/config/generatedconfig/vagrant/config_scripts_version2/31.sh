#!/bin/sh
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR
if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s31"
ovs-vsctl --no-wait del-br s31

echo "Adding s31"
ovs-vsctl --no-wait --may-exist add-br s31 -- set bridge s31 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s31 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s31 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s31 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s31 eth4 -- set Interface eth4 ofport_request=4


echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s31 protocols=OpenFlow11
sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s31 table=0

echo "adding the sample flow on s31"
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:3101,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:3102,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:3103,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:3104,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:3105,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:3106,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.31/32,actions=push_verify,set_verify_rule:3107,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.31/32,actions=push_verify,set_verify_rule:3108,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.31/32,actions=push_verify,set_verify_rule:3109,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.32/32,actions=push_verify,set_verify_rule:3110,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.32/32,actions=push_verify,set_verify_rule:3111,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.32/32,actions=push_verify,set_verify_rule:3112,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.41/32,actions=push_verify,set_verify_rule:3113,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.41/32,actions=push_verify,set_verify_rule:3114,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.3.41/32,actions=push_verify,set_verify_rule:3115,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.42/32,actions=push_verify,set_verify_rule:3116,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.42/32,actions=push_verify,set_verify_rule:3117,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.3.42/32,actions=push_verify,set_verify_rule:3118,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.23/32,actions=push_verify,set_verify_rule:3119,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.23/32,actions=push_verify,set_verify_rule:3120,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.23/32,actions=push_verify,set_verify_rule:3121,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.24/32,actions=push_verify,set_verify_rule:3122,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.24/32,actions=push_verify,set_verify_rule:3123,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.24/32,actions=push_verify,set_verify_rule:3124,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.61/32,actions=push_verify,set_verify_rule:3125,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.61/32,actions=push_verify,set_verify_rule:3126,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.61/32,actions=push_verify,set_verify_rule:3127,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.62/32,actions=push_verify,set_verify_rule:3128,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.62/32,actions=push_verify,set_verify_rule:3129,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.62/32,actions=push_verify,set_verify_rule:3130,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:3131,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:3132,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:3133,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:3134,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:3135,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:3136,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:3137,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:3138,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:3139,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:3140,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:3141,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:3142,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3143,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3144,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s31 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:3145,set_verify_port=1,output:2

echo "dumping the flow from s31"
ovs-ofctl -O OpenFlow11 dump-flows s31
sleep 8s

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 172.16.3.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 172.16.3.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4

echo "Adding correct routes"
sudo route add -net 172.16.0.0 netmask 255.255.255.248  dev eth1
sudo route add -net 172.16.0.8 netmask 255.255.255.248  dev eth2
sudo route add -net 172.16.3.31 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.3.32 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.3.23 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.3.24 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.3.41 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.3.42 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.0.61 netmask 255.255.255.255 gw 172.16.3.41 dev eth4
sudo route add -net 172.16.0.62 netmask 255.255.255.255 gw 172.16.3.41 dev eth4
sudo route add -net 172.16.0.16 netmask 255.255.255.248 gw 172.16.3.41 dev eth4
sudo route add -net 172.16.0.24 netmask 255.255.255.255 gw 172.16.3.41 dev eth4
sudo route add -net 172.16.0.31 netmask 255.255.255.255 gw 172.16.3.41 dev eth4
sudo route add -net 172.16.0.32 netmask 255.255.255.252 gw 172.16.3.41 dev eth4
sudo route add -net 172.16.0.0 netmask 255.255.0.0 gw 172.16.13  dev eth2

