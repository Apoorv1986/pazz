#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting S22"
ovs-vsctl --no-wait del-br s22

echo "Adding S22"
ovs-vsctl --no-wait --may-exist add-br s22 -- set bridge s22 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s22 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s22 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s22 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s22 eth4 -- set Interface eth4 ofport_request=4



echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s22 protocols=OpenFlow11

sleep 10

echo "delelting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s22 table=0

echo "adding the sample flow on S22"
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.22/32,actions=push_verify,set_verify_rule:2201,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.22/32,actions=push_verify,set_verify_rule:2202,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.22/32,actions=push_verify,set_verify_rule:2203,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.32/32,actions=push_verify,set_verify_rule:2204,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.32/32,actions=push_verify,set_verify_rule:2205,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.32/32,actions=push_verify,set_verify_rule:2206,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.31/32,actions=push_verify,set_verify_rule:2207,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.31/32,actions=push_verify,set_verify_rule:2208,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.31/32,actions=push_verify,set_verify_rule:2209,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.32/32,actions=push_verify,set_verify_rule:2210,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.32/32,actions=push_verify,set_verify_rule:2211,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.32/32,actions=push_verify,set_verify_rule:2212,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2213,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2214,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2215,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:2216,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:2217,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:2218,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.13/32,actions=push_verify,set_verify_rule:2219,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.13/32,actions=push_verify,set_verify_rule:2220,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.13/32,actions=push_verify,set_verify_rule:2221,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.14/32,actions=push_verify,set_verify_rule:2222,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.14/32,actions=push_verify,set_verify_rule:2223,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.14/32,actions=push_verify,set_verify_rule:2224,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.211/32,actions=push_verify,set_verify_rule:2225,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.211/32,actions=push_verify,set_verify_rule:2226,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.211/32,actions=push_verify,set_verify_rule:2227,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.212/32,actions=push_verify,set_verify_rule:2228,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.212/32,actions=push_verify,set_verify_rule:2229,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.212/32,actions=push_verify,set_verify_rule:2230,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:2231,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:2232,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:2233,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:2234,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:2235,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:2236,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2237,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2238,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s22 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:2239,set_verify_port=1,output:1

echo "dumping the flow from S22"
ovs-ofctl -O OpenFlow11 dump-flows s22
sleep 8s

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 172.16.2.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 172.16.2.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4

echo "Adding routes"
sudo route add -net 172.16.0.22 netmask 255.255.255.255  dev eth1
sudo route add -net 172.16.0.32 netmask 255.255.255.255  dev eth2
sudo route add -net 172.16.2.31 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.2.32 netmask 255.255.255.252  dev eth3
sudo route add -net 172.16.2.41 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.2.42 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.2.13 netmask 255.255.255.255 gw 172.16.2.32  dev eth3
sudo route add -net 172.16.2.14 netmask 255.255.255.255 gw 172.16.2.32  dev eth3
sudo route add -net 172.16.0.211 netmask 255.255.255.255 gw 172.16.2.42 dev eth4
sudo route add -net 172.16.0.212 netmask 255.255.255.255 gw 172.16.2.42 dev eth4
sudo route add -net 172.16.0.0 netmask 255.255.255.248 gw 172.16.2.32 dev eth3
sudo route add -net 172.16.0.8 netmask 255.255.255.248 gw 172.16.2.32 dev eth3
sudo route add -net 172.16.0.0 netmask 255.255.0.0 gw 172.16.0.22  dev eth1
