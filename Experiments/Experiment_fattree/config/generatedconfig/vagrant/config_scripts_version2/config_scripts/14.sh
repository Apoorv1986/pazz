#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting S14"
ovs-vsctl --no-wait del-br s14

echo "Adding S14"
ovs-vsctl --no-wait --may-exist add-br s14 -- set bridge s14 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s14 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s14 eth2 -- set Interface eth2 ofport_request=2

echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s14 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s14 table=0


echo "adding the sample flow on S14"
ovs-ofctl -O OpenFlow11 add-flow s14 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:1401,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s14 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:1402,set_verify_port=1,output:1

echo "dumping the flow from S14"
ovs-ofctl -O OpenFlow11 dump-flows s14

sleep 8

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.1.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.1.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2

echo "Adding Routes"
sudo route add -net 172.16.1.13 netmask 255.255.255.255 dev eth1
sudo route add -net 172.16.1.14 netmask 255.255.255.255 dev eth1
sudo route add -net 172.16.1.24 netmask 255.255.255.255 dev eth2
sudo route add -net 172.16.0.111 netmask 255.255.255.255 gw 172.16.1.14 dev eth1
sudo route add -net 172.16.0.112 netmask 255.255.255.255 gw 172.16.1.14 dev eth1
sudo route add -net 172.16.1.31 netmask 255.255.255.255 gw 172.16.1.14 dev eth1
sudo route add -net 172.16.1.32 netmask 255.255.255.255 gw 172.16.1.14 dev eth1
sudo route add -net 172.16.0.0 netmask 255.255.0.0 gw 172.16.1.24 dev eth2
