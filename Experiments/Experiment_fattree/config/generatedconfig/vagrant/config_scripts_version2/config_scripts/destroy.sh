#!/bin/bash

echo "Destroying S0"
vagrant destroy -f s0

echo "Destroying S1"
vagrant destroy -f s1

echo "Destroying S2"
vagrant destroy -f s2

echo "Destroying S3"
vagrant destroy -f s3

echo "Destroying S11"
vagrant destroy -f s11

echo "Destroying S12"
vagrant destroy -f s12

echo "Destroying S13"
vagrant destroy -f s13

echo "Destroying S14"
vagrant destroy -f s14

echo "Destroying S21"
vagrant destroy -f s21

echo "Destroying S22"
vagrant destroy -f s22

echo "Destroying S23"
vagrant destroy -f s23

echo "Destroying S24"
vagrant destroy -f s24

echo "Destroying S31"
vagrant destroy -f s31

echo "Destroying S32"
vagrant destroy -f s32

echo "Destroying S33"
vagrant destroy -f s33

echo "Destroying S34"
vagrant destroy -f s34

echo "Destroying S41"
vagrant destroy -f s41

echo "Destroying S42"
vagrant destroy -f s42

echo "Destroying S43"
vagrant destroy -f s43

echo "Destroying S44"
vagrant destroy -f s44

echo "Destroying S30"
#vagrant destroy -f s30

echo "Destroying gen20"
#vagrant destroy -f gen20

