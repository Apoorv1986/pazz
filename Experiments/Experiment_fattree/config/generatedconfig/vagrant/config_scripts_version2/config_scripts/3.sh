#!/bin/sh
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR
if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting S3"
ovs-vsctl --no-wait del-br s3

echo "Adding S3"
ovs-vsctl --no-wait --may-exist add-br s3 -- set bridge s3 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s3 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s3 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s3 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s3 eth4 -- set Interface eth4 ofport_request=4


echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s3 protocols=OpenFlow11
sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s3 table=0

echo "adding the sample flow on S3"
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:301,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:302,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:303,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.222/32,actions=push_verify,set_verify_rule:304,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.222/32,actions=push_verify,set_verify_rule:305,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.222/32,actions=push_verify,set_verify_rule:306,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.62/32, actions=push_verify,set_verify_rule:307,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.62/32, actions=push_verify,set_verify_rule:308,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.62/32, actions=push_verify,set_verify_rule:309,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.162/32,actions=push_verify,set_verify_rule:310,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.162/32,actions=push_verify,set_verify_rule:311,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.162/32,actions=push_verify,set_verify_rule:312,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:313,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:314,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:315,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.112/32,actions=push_verify,set_verify_rule:316,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.112/32,actions=push_verify,set_verify_rule:317,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.112/32,actions=push_verify,set_verify_rule:318,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:319,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:320,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:321,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.48/29 ,actions=push_verify,set_verify_rule:322,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.48/29 ,actions=push_verify,set_verify_rule:323,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.48/29 ,actions=push_verify,set_verify_rule:324,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.128/26,actions=push_verify,set_verify_rule:325,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.128/26,actions=push_verify,set_verify_rule:326,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.128/26,actions=push_verify,set_verify_rule:327,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.0/24,  actions=push_verify,set_verify_rule:328,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.1.0/24,  actions=push_verify,set_verify_rule:329,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.1.0/24,  actions=push_verify,set_verify_rule:330,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.0/24,  actions=push_verify,set_verify_rule:331,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.0/24,  actions=push_verify,set_verify_rule:332,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.0/24,  actions=push_verify,set_verify_rule:333,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.0/24,  actions=push_verify,set_verify_rule:334,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.0/24,  actions=push_verify,set_verify_rule:335,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.0/24,  actions=push_verify,set_verify_rule:336,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.0/24,  actions=push_verify,set_verify_rule:337,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.0/24,  actions=push_verify,set_verify_rule:338,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.4.0/24,  actions=push_verify,set_verify_rule:339,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/29,  actions=push_verify,set_verify_rule:340,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/29,  actions=push_verify,set_verify_rule:341,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/29,  actions=push_verify,set_verify_rule:342,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,  actions=push_verify,set_verify_rule:343,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.8/29,  actions=push_verify,set_verify_rule:344,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,  actions=push_verify,set_verify_rule:345,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.16/29, actions=push_verify,set_verify_rule:346,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.16/29, actions=push_verify,set_verify_rule:347,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.16/29, actions=push_verify,set_verify_rule:348,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.24/30, actions=push_verify,set_verify_rule:349,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.24/30, actions=push_verify,set_verify_rule:350,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.24/30, actions=push_verify,set_verify_rule:351,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.21.0/24, actions=push_verify,set_verify_rule:352,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.21.0/24, actions=push_verify,set_verify_rule:353,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.21.0/24, actions=push_verify,set_verify_rule:354,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.5.0/24, actions=push_verify,set_verify_rule:355,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.0/24, actions=push_verify,set_verify_rule:356,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s3 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.5.0/24, actions=push_verify,set_verify_rule:357,set_verify_port=1,output:2

echo "dumping the flow from S3"
ovs-ofctl -O OpenFlow11 dump-flows s3

sleep 8s

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4

echo "Adding routes"
sudo route add -net 172.16.0.122 netmask 255.255.255.255 dev eth1
sudo route add -net 172.16.0.222 netmask 255.255.255.255 dev eth2
sudo route add -net 172.16.0.62  netmask 255.255.255.255 dev eth3
sudo route add -net 172.16.0.162 netmask 255.255.255.255 dev eth4
sudo route add -net 172.16.0.111 netmask 255.255.255.255 gw 172.16.0.122 dev eth1
sudo route add -net 172.16.0.112 netmask 255.255.255.255 gw 172.16.0.122 dev eth1
sudo route add -net 172.16.0.192 netmask 255.255.255.192 gw 172.16.0.222 dev eth2
sudo route add -net 172.16.0.48  netmask 255.255.255.240 gw 172.16.0.62  dev eth3
sudo route add -net 172.16.0.128 netmask 255.255.255.192 gw 172.16.0.162 dev eth4
sudo route add -net 172.16.1.0 netmask 255.255.255.0 gw 172.16.0.122 dev eth1
sudo route add -net 172.16.2.0 netmask 255.255.255.0 gw 172.16.0.222 dev eth2
sudo route add -net 172.16.3.0 netmask 255.255.255.0 gw 172.16.0.62  dev eth3
sudo route add -net 172.16.4.0 netmask 255.255.255.0 gw 172.16.0.162 dev eth4
sudo route add -net 172.16.0.0 netmask 255.255.255.248 gw 172.16.0.122 dev eth1
sudo route add -net 172.16.0.8 netmask 255.255.255.248 gw 172.16.0.62 dev eth3
sudo route add -net 172.16.0.16 netmask 255.255.255.248 gw 172.16.0.122 dev eth1
sudo route add -net 172.16.0.24 netmask 255.255.255.252 gw 172.16.0.122 dev eth1
sudo route add -net 172.16.21.0 netmask 255.255.255.0 gw 172.16.0.122 dev eth1
sudo route add -net 172.16.5.0 netmask 255.255.255.0 gw 172.16.0.222 dev eth2

