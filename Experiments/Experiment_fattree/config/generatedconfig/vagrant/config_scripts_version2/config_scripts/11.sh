#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s11"
ovs-vsctl --no-wait del-br s11

echo "Adding s11"
ovs-vsctl --no-wait --may-exist add-br s11 -- set bridge s11 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s11 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s11 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s11 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s11 eth4 -- set Interface eth4 ofport_request=4


echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s11 protocols=OpenFlow11

sleep 10

echo "delelting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s11 table=0

echo "adding the sample flow on s11"
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:1101,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:1102,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/29,actions=push_verify,set_verify_rule:1103,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:1104,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:1105,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,actions=push_verify,set_verify_rule:1106,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.1.31/32,actions=push_verify,set_verify_rule:1107,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.31/32,actions=push_verify,set_verify_rule:1108,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.1.31/32,actions=push_verify,set_verify_rule:1109,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.1.32/32,actions=push_verify,set_verify_rule:1110,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.32/32,actions=push_verify,set_verify_rule:1111,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.1.32/32,actions=push_verify,set_verify_rule:1112,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.1.41/32,actions=push_verify,set_verify_rule:1113,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.41/32,actions=push_verify,set_verify_rule:1114,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.1.41/32,actions=push_verify,set_verify_rule:1115,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.1.42/32,actions=push_verify,set_verify_rule:1116,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.42/32,actions=push_verify,set_verify_rule:1117,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.1.42/32,actions=push_verify,set_verify_rule:1118,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.1.23/32,actions=push_verify,set_verify_rule:1119,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.23/32,actions=push_verify,set_verify_rule:1120,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.1.23/32,actions=push_verify,set_verify_rule:1121,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.1.24/32,actions=push_verify,set_verify_rule:1122,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.24/32,actions=push_verify,set_verify_rule:1123,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.1.24/32,actions=push_verify,set_verify_rule:1124,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.121/32,actions=push_verify,set_verify_rule:1125,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.121/32,actions=push_verify,set_verify_rule:1126,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.121/32,actions=push_verify,set_verify_rule:1127,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:1128,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:1129,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:1130,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:1131,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:1132,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.16/29,actions=push_verify,set_verify_rule:1133,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:1134,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:1135,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.24/32,actions=push_verify,set_verify_rule:1136,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:1137,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:1138,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.31/32,actions=push_verify,set_verify_rule:1139,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:1140,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:1141,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.32/30,actions=push_verify,set_verify_rule:1142,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s11 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:1143,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s11 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:1144,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s11 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/16,actions=push_verify,set_verify_rule:1145,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.21.0/24,actions=push_verify,set_verify_rule:1146,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.21.0/24,actions=push_verify,set_verify_rule:1147,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s11 priority=15,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.21.0/24,actions=push_verify,set_verify_rule:1148,set_verify_port=1,output:3

echo "dumping the flow from s11"
ovs-ofctl -O OpenFlow11 dump-flows s11

sleep 8

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 172.16.1.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 172.16.1.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4

echo "Adding correct routes"
sudo route add -net 172.16.0.0  netmask 255.255.255.248  dev eth1
sudo route add -net 172.16.0.8  netmask 255.255.255.248  dev eth2
sudo route add -net 172.16.1.31 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.1.32 netmask 255.255.255.255  dev eth3
sudo route add -net 172.16.1.41 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.1.42 netmask 255.255.255.255  dev eth4
sudo route add -net 172.16.1.23 netmask 255.255.255.255 gw 172.16.1.31  dev eth3
sudo route add -net 172.16.1.24 netmask 255.255.255.255 gw 172.16.1.31  dev eth3
sudo route add -net 172.16.0.121 netmask 255.255.255.255 gw 172.16.1.41 dev eth4
sudo route add -net 172.16.0.122 netmask 255.255.255.255 gw 172.16.1.41 dev eth4
sudo route add -net 172.16.0.16 netmask 255.255.255.248 gw 172.16.1.41 dev eth4
sudo route add -net 172.16.0.24 netmask 255.255.255.255 gw 172.16.1.41 dev eth4
sudo route add -net 172.16.0.31 netmask 255.255.255.255 gw 172.16.1.41 dev eth4
sudo route add -net 172.16.0.32 netmask 255.255.255.252 gw 172.16.1.41 dev eth4
sudo route add -net 172.16.0.0 netmask 255.255.0.0 gw 172.16.0.2  dev eth1
sudo route add -net 172.16.21.0 netmask 255.255.255.0 gw 172.16.1.31 dev eth3
