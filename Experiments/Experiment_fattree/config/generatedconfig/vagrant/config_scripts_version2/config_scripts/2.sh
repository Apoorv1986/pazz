#!/bin/bash
#Author = Said Jawad Saidi ||  Zsolt Vági

OVSDB_DIR="/usr/local/etc/openvswitch/"
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance"
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server"
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl"
ovs-vsctl --no-wait init

echo "Deleting s2"
ovs-vsctl --no-wait del-br s2

echo "Adding s2"
ovs-vsctl --no-wait --may-exist add-br s2 -- set bridge s2 datapath_type=netdev

echo "Adding ports"

ovs-vsctl --no-wait --may-exist add-port s2 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s2 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s2 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s2 eth4 -- set Interface eth4 ofport_request=4
ovs-vsctl --no-wait --may-exist add-port s2 eth5 -- set Interface eth5 ofport_request=5



echo "Starting ovs-vswitch"
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach &

ovs-vsctl --no-wait set bridge s2 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s2 table=0

echo "adding the sample flow on S2"
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.121/32,actions=push_verify,set_verify_rule:201,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.121/32,actions=push_verify,set_verify_rule:202,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.121/32,actions=push_verify,set_verify_rule:203,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:204,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:205,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.122/32,actions=push_verify,set_verify_rule:206,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.221/32,actions=push_verify,set_verify_rule:207,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.221/32,actions=push_verify,set_verify_rule:208,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.221/32,actions=push_verify,set_verify_rule:209,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.61/32, actions=push_verify,set_verify_rule:210,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.61/32, actions=push_verify,set_verify_rule:211,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.61/32, actions=push_verify,set_verify_rule:212,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.161/32,actions=push_verify,set_verify_rule:213,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.161/32,actions=push_verify,set_verify_rule:214,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.161/32,actions=push_verify,set_verify_rule:215,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:216,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:217,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.111/32,actions=push_verify,set_verify_rule:218,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.112/32,actions=push_verify,set_verify_rule:219,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.112/32,actions=push_verify,set_verify_rule:220,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.112/32,actions=push_verify,set_verify_rule:221,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:222,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:223,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.192/26,actions=push_verify,set_verify_rule:224,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.48/28, actions=push_verify,set_verify_rule:225,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.48/28, actions=push_verify,set_verify_rule:226,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.48/28, actions=push_verify,set_verify_rule:227,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.128/26,actions=push_verify,set_verify_rule:228,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.128/26,actions=push_verify,set_verify_rule:229,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.128/26,actions=push_verify,set_verify_rule:230,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.1.0/24,  actions=push_verify,set_verify_rule:231,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.1.0/24,  actions=push_verify,set_verify_rule:232,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.1.0/24,  actions=push_verify,set_verify_rule:233,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.0/24,  actions=push_verify,set_verify_rule:234,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.0/24,  actions=push_verify,set_verify_rule:235,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.2.0/24,  actions=push_verify,set_verify_rule:236,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.3.0/24,  actions=push_verify,set_verify_rule:237,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.3.0/24,  actions=push_verify,set_verify_rule:238,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.3.0/24,  actions=push_verify,set_verify_rule:239,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.4.0/24,  actions=push_verify,set_verify_rule:240,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.4.0/24,  actions=push_verify,set_verify_rule:241,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.4.0/24,  actions=push_verify,set_verify_rule:242,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.0/29,  actions=push_verify,set_verify_rule:243,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.0.0/29,  actions=push_verify,set_verify_rule:244,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.0/29,  actions=push_verify,set_verify_rule:245,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.8/29,  actions=push_verify,set_verify_rule:246,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.8/29,  actions=push_verify,set_verify_rule:247,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.8/29,  actions=push_verify,set_verify_rule:248,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.31/32, actions=push_verify,set_verify_rule:249,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.31/32, actions=push_verify,set_verify_rule:250,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.31/32, actions=push_verify,set_verify_rule:251,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.0.32/32, actions=push_verify,set_verify_rule:252,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.0.32/32, actions=push_verify,set_verify_rule:253,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.0.32/32, actions=push_verify,set_verify_rule:254,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.21.0/24, actions=push_verify,set_verify_rule:255,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.21.0/24, actions=push_verify,set_verify_rule:256,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.21.0/24, actions=push_verify,set_verify_rule:257,set_verify_port=1,output:1
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.5.0/24, actions=push_verify,set_verify_rule:258,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.0/24, actions=push_verify,set_verify_rule:259,set_verify_port=1,output:2
ovs-ofctl -O OpenFlow11 add-flow s2 priority=10,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.5.0/24, actions=push_verify,set_verify_rule:260,set_verify_port=1,output:2


echo "dumping the flow from S2"
ovs-ofctl -O OpenFlow11 dump-flows s2


sleep 8s

echo "Removing default routes"
sudo route del -net 0.0.0.0  netmask 0.0.0.0 gw 10.0.2.2  dev eth0
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth1
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth2
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth3
sudo route del -net 172.16.0.0  netmask 255.255.255.0 gw 0.0.0.0  dev eth4

echo "Adding routes"
sudo route add -net 172.16.0.121 netmask 255.255.255.255 dev eth1
sudo route add -net 172.16.0.122 netmask 255.255.255.255 dev eth1
sudo route add -net 172.16.0.221 netmask 255.255.255.255 dev eth2
sudo route add -net 172.16.0.61  netmask 255.255.255.255 dev eth3
sudo route add -net 172.16.0.161 netmask 255.255.255.255 dev eth4
sudo route add -net 172.16.0.111 netmask 255.255.255.255 gw 172.16.0.121 dev eth1
sudo route add -net 172.16.0.112 netmask 255.255.255.255 gw 172.16.0.121 dev eth1
sudo route add -net 172.16.0.192 netmask 255.255.255.192 gw 172.16.0.221 dev eth2
sudo route add -net 172.16.0.48  netmask 255.255.255.240 gw 172.16.0.61  dev eth3
sudo route add -net 172.16.0.128 netmask 255.255.255.192 gw 172.16.0.161 dev eth4
sudo route add -net 172.16.1.0 netmask 255.255.255.0 gw 172.16.0.121 dev eth1
sudo route add -net 172.16.2.0 netmask 255.255.255.0 gw 172.16.0.221 dev eth2
sudo route add -net 172.16.3.0 netmask 255.255.255.0 gw 172.16.0.61  dev eth3
sudo route add -net 172.16.4.0 netmask 255.255.255.0 gw 172.16.0.161 dev eth4
sudo route add -net 172.16.0.0 netmask 255.255.255.248 gw 172.16.0.121 dev eth1
sudo route add -net 172.16.0.8 netmask 255.255.255.248 gw 172.16.0.61 dev eth3
sudo route add -net 172.16.0.31 netmask 255.255.255.255 gw 172.16.0.61 dev eth3
sudo route add -net 172.16.0.32 netmask 255.255.255.252 gw 172.16.0.61 dev eth3
sudo route add -net 172.16.21.0 netmask 255.255.255.0 gw 172.16.0.121 dev eth1
sudo route add -net 172.16.5.0 netmask 255.255.255.0 gw 172.16.0.221 dev eth2

