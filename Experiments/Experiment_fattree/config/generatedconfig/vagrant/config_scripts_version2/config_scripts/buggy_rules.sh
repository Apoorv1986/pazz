#!/bin/bash
#Author = Zsolt Vagi
sudo timedatectl set-timezone Europe/Berlin

echo "Time when buggy rules get added"
date +"%Y-%m-%d %H:%M:%S,%3N" > /vagrant/config_scripts/BuggyRules/rules_timestamp.txt

echo "Adding buggy rules to S0"
vagrant ssh s0  -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s0.sh' &

echo "Adding buggy rules to S1"
vagrant ssh s1  -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s1.sh' &

echo "Adding buggy rules to S2"
vagrant ssh s2  -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s2.sh' &

echo "Adding buggy rules to S3"
vagrant ssh s3  -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./Buggy_rules_s3.sh' &

echo "Adding buggy rules to S11"
vagrant ssh s11 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s11.sh' &

echo "Adding buggy rules to S12"
vagrant ssh s12 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s12.sh' &

echo "Adding buggy rules to S13"
vagrant ssh s13 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s13.sh' &

echo "Adding buggy rules to S21"
vagrant ssh s21 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s21.sh' &

echo "Adding buggy rules to S22"
vagrant ssh s22 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s22.sh' &

echo "Adding buggy rules to S23"
vagrant ssh s23 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s23.sh' &

echo "Adding buggy rules to S31"
vagrant ssh s31 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s31.sh' &

echo "Adding buggy rules to S42"
vagrant ssh s42 -c 'cd /vagrant/config_scripts/BuggyRules; sudo -E ./buggy_rules_s42.sh' &
