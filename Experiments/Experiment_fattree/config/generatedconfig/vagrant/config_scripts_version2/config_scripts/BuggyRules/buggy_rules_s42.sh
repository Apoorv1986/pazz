#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s42 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.5.53/32,actions=push_verify,set_verify_rule:4202,set_verify_port=1,output:1

echo "dumping the flow from s42"
ovs-ofctl -O OpenFlow11 dump-flows s42

sleep 8s
