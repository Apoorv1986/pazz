#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s31 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.23/32,actions=push_verify,set_verify_rule:3102,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s31 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.2.24/32,actions=push_verify,set_verify_rule:3103,set_verify_port=1,output:1


echo "dumping the flow from s31"
ovs-ofctl -O OpenFlow11 dump-flows s31

sleep 8s
