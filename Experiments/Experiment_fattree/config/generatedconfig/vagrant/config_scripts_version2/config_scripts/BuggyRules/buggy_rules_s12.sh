#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s12 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.51/32,actions=push_verify,set_verify_rule:1202,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s12 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.52/32,actions=push_verify,set_verify_rule:1203,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s12 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.53/32,actions=push_verify,set_verify_rule:1204,set_verify_port=1,output:2

echo "dumping the flow from s12"
ovs-ofctl -O OpenFlow11 dump-flows s12

sleep 8s
