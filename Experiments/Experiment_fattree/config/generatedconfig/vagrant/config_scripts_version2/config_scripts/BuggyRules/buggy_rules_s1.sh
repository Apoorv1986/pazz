#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s1 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:102,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s1 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:103,set_verify_port=1,output:2

echo "dumping the flow from s1"
ovs-ofctl -O OpenFlow11 dump-flows s1

sleep 8s
