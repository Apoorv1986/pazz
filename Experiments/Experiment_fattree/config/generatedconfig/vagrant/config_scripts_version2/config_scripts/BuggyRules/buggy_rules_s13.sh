#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:1302,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:1303,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.51/32,actions=push_verify,set_verify_rule:1304,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.52/32,actions=push_verify,set_verify_rule:1305,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.53/32,actions=push_verify,set_verify_rule:1306,set_verify_port=1,output:2

#Buggy rules for unused address space
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=1.0.0.0/8,actions=push_verify,set_verify_rule:1311,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=2.0.0.0/8,actions=push_verify,set_verify_rule:1312,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=3.0.0.0/8,actions=push_verify,set_verify_rule:1313,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=4.0.0.0/8,actions=push_verify,set_verify_rule:1314,set_verify_port=1,output:2

echo "dumping the flow from s13"
ovs-ofctl -O OpenFlow11 dump-flows s13

sleep 8s
