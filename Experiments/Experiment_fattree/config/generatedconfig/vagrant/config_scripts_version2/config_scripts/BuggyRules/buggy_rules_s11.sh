#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:1102,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s11 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:1103,set_verify_port=1,output:2


echo "dumping the flow from s11"
ovs-ofctl -O OpenFlow11 dump-flows s11

sleep 8s
