#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s2 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.5.51/32,actions=push_verify,set_verify_rule:202,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s2 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.5.52/32,actions=push_verify,set_verify_rule:203,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s2 priority=20,table=0,in_port=4,dl_type=0x0800,nw_dst=172.16.5.53/32,actions=push_verify,set_verify_rule:204,set_verify_port=1,output:2

echo "dumping the flow from s2"
ovs-ofctl -O OpenFlow11 dump-flows s2

sleep 8s
