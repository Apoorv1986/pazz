#!/bin/bash
#Author = Zsolt Vagi ||  Said Jawad Saidi || Seifeddine Fathalli
sudo timedatectl set-timezone Europe/Berlin

echo "Time when buggy rules get added"
date +"%Y-%m-%d %H:%M:%S,%3N" > /vagrant/config_scripts/BuggyRules/addingRules.txt

#Buggy rules for production related traffic 
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=7,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.23/32,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=7,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.24/32,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=7,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.51/32,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=7,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.51/32,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=7,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.5.51/32,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:2

#Buggy rules for unused address space
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=1.0.0.0/8,actions=push_verify,set_verify_rule:11,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=2.0.0.0/8,actions=push_verify,set_verify_rule:11,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=3.0.0.0/8,actions=push_verify,set_verify_rule:11,set_verify_port=1,output:2
sudo ovs-ofctl -O OpenFlow11 add-flow s13 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=4.0.0.0/8,actions=push_verify,set_verify_rule:11,set_verify_port=1,output:2

echo "dumping the flow from s0"
ovs-ofctl -O OpenFlow11 dump-flows s13

sleep 8s
