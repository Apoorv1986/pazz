#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s3 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.5.53/32,actions=push_verify,set_verify_rule:302,set_verify_port=1,output:4

echo "dumping the flow from s3"
ovs-ofctl -O OpenFlow11 dump-flows s3

sleep 8s
