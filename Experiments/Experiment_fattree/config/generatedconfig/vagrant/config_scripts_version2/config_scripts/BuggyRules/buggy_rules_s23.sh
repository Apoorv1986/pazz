#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.5.51/32,actions=push_verify,set_verify_rule:2302,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=2,dl_type=0x0800,nw_dst=172.16.5.52/32,actions=push_verify,set_verify_rule:2303,set_verify_port=1,output:1
sudo ovs-ofctl -O OpenFlow11 add-flow s23 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2304,set_verify_port=1,output:2

echo "dumping the flow from s23"
ovs-ofctl -O OpenFlow11 dump-flows s23

sleep 8s
