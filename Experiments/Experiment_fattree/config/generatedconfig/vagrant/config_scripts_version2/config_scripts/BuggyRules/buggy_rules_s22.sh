#!/bin/bash
#Author = Zsolt Vagi

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.23/32,actions=push_verify,set_verify_rule:2202,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.24/32,actions=push_verify,set_verify_rule:2203,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.5.51/32,actions=push_verify,set_verify_rule:2204,set_verify_port=1,output:4
sudo ovs-ofctl -O OpenFlow11 add-flow s22 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2205,set_verify_port=1,output:4

echo "dumping the flow from s22"
ovs-ofctl -O OpenFlow11 dump-flows s22

sleep 8s
