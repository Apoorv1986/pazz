#!/bin/bash
#Author = Zsolt Vagi
sudo timedatectl set-timezone Europe/Berlin

echo "Time when buggy rules get added"
date +"%Y-%m-%d %H:%M:%S,%3N" > /vagrant/config_scripts/BuggyRules/addingRules.txt

#Buggy rules for production related traffic
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=1,dl_type=0x0800,nw_dst=172.16.2.41/32,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:3
sudo ovs-ofctl -O OpenFlow11 add-flow s0 priority=20,table=0,in_port=3,dl_type=0x0800,nw_dst=172.16.2.42/32,actions=push_verify,set_verify_rule:3,set_verify_port=1,output:2


echo "dumping the flow from s0"
ovs-ofctl -O OpenFlow11 dump-flows s0

sleep 8s
