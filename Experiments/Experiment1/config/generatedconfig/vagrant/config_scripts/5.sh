#!/bin/bash
export https_proxy=https://:@proxy.routerlab:8080
export http_proxy=http://:@proxy.routerlab:8080
pip install pbr
pip install ryu

python /vagrant/config_scripts/packet_gen.py