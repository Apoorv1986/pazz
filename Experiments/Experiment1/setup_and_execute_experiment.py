import argparse
import os, Queue
import threading, sys
import time
from subprocess import PIPE, Popen

import psutil

pazz_path = "/Users/saidjawadsaidi/repo/Pazz/"
config_path = "/Users/saidjawadsaidi/repo/Pazz/Experiments/Experiment1/config/"


def setup_experiment():
    try:
        print "Generating Configuration"
        os.chdir(pazz_path + "utils/ConfigurationConverter/build")
        print "current directory is", os.path.realpath(os.curdir)
        config_converter = Popen(['java', '-jar', 'ConfigMain.jar', config_path], stdout=PIPE, bufsize=10485760)
        for line in config_converter.stdout:
            print line
    except:
        print Exception.message
        exit(-1)
    try:
        print "Generating Virutal Machines"
        os.chdir(pazz_path + "utils/VMGenerator")
        vm_generator = Popen(['python', 'VMGenerator.py', '-p', config_path], stdout=PIPE, bufsize=10485760)
        for line in vm_generator.stdout:
            print line
    except:
        print "Building virtual machines failed"
        exit(-1)


def _read_output(pipe, queue):
    while True:
        line = pipe.readline()
        queue.put(line)


def execute_controlplane():
    __cleanup()
    consistency_tester = None
    controlplane_component = None
    ct_queue = Queue.Queue()
    cp_queue = Queue.Queue()
    try:
        print "Starting the Control Plane Component"
        os.chdir(pazz_path + "PazzControlPlane/ant/build")
        controlplane_component = Popen(['java', '-jar', 'PazzMain.jar', '-p', config_path, '-h', '32'], stdout=PIPE, \
                                       universal_newlines=True, bufsize=10000)
    except:
        print "executing control plane component/PathServer failed"
        exit(-1)

    try:
        # wait for some time until the control plane component starts up
        time.sleep(10)
        print "Starting Consistency Tester"
        os.chdir(pazz_path + "ConsistencyTester")
        consistency_tester = Popen(['stdbuf', '-o0', 'python', 'ConsistencyTester.py'], stdout=PIPE, universal_newlines=True,
                                   bufsize=10000)
    except Exception, e:
        print 'Execution of Consistency Tester Failed:%s' % (str(e))
        exit(-1)
    cpthread = threading.Thread(target=_read_output, args=(controlplane_component.stdout, cp_queue))
    ctthread = threading.Thread(target=_read_output, args=(consistency_tester.stdout, ct_queue))
    ctthread.daemon = True
    cpthread.daemon = True
    cpthread.start()
    ctthread.start()
    while True:
        controlplane_component.poll()
        consistency_tester.poll()

        if controlplane_component.returncode is not None or consistency_tester.returncode is not None:
            __cleanup()
            break
        try:
            cp_output = cp_queue.get(False)
            sys.stdout.write("Control Plane Component: %s\n" % (cp_output))
        except Queue.Empty:
            pass
        try:
            ct_output = ct_queue.get(False)
            sys.stdout.write("Consistency Tester: %s\n"%(ct_output))
        except Queue.Empty:
            pass



def execute_dataplane():
    __drop_privileges()
    try:
        print "Starting the VMs(Topology)"
        os.chdir(config_path + "generatedconfig/vagrant")
        topology = Popen(['vagrant', 'up', '--provision'])
    except:
        print "Starting the VMs Failed"
        exit(-1)


def execute_experiment():
    cpethread = threading.Thread(target=execute_controlplane)
    cpethread.start()
    execute_dataplane()
    cpethread.join()


def __drop_privileges(uid_name='routerlab', gid_name='routerlab'):
    '''
    source: http://antonym.org/2005/12/dropping-privileges-in-python.html
    '''
    import os, pwd, grp

    starting_uid = os.getuid()
    starting_gid = os.getgid()

    starting_uid_name = pwd.getpwuid(starting_uid)[0]

    if os.getuid() != 0:
        # We're not root so, like, whatever dude
        return

    # If we started as root, drop privs and become the specified user/group
    if starting_uid == 0:

        # Get the uid/gid from the name
        running_uid = pwd.getpwnam(uid_name)[2]
        running_gid = grp.getgrnam(gid_name)[2]

        # Try setting the new uid/gid
        try:
            os.setgid(running_gid)
        except OSError, e:
            pass

        try:
            os.setuid(running_uid)
        except OSError, e:
            pass


def __cleanup():
    try:
        print 'Terminating possibly remaining processes from previous executions'
        for proc in psutil.process_iter():
            for pcon in proc.connections(kind='all'):
                #print pcon
                if pcon.laddr and (pcon.laddr[1] == 6343 or pcon.laddr[1] == 9090):
                    print "killing zombie process %s from previous executions" % (proc.pid)
                    proc.terminate()
    except:
        print 'failed to perform clean up, make sure that you execute the experiment with root privileges'


def _parse_argumetns():
    parser = argparse.ArgumentParser(description="Experiment Setup and Executor")
    parser.add_argument('-p', action='store', dest='path')
    parser.add_argument('-c', action='store', dest='config')
    parser.add_argument('-e', "--execute", action='store_true')
    parser.add_argument('-s', "--setup", action='store_true')
    parser.add_argument('-d', "--delete", action='store_true')
    parser.add_argument('-o', "--cponly", action='store_true')
    parsed = parser.parse_args()
    return parsed


if __name__ == "__main__":

    parsed = _parse_argumetns()
    if not (os.path.exists(parsed.path) and os.path.exists(parsed.config)):
        print "Path to Pazz executables or Experiment Configuration does not exist"
        exit(-1)
    if not parsed.path.endswith("/"):
        parsed.path += "/"
    if not parsed.config.endswith("/"):
        parsed.config += "/"
    global pazz_path
    pazz_path = parsed.path

    global config_path
    config_path = parsed.config

    if parsed.delete:
        __cleanup()
    elif parsed.cponly:
        execute_controlplane()
    elif parsed.setup:
        setup_experiment()
    elif parsed.execute:
        execute_experiment()
    elif not parsed.setup and not parsed.execute:
        setup_experiment()
        execute_experiment()
