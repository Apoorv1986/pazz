#!/bin/bash


echo "Erasing the table0, aka removing the defaut flow with actions=Normal"
 ovs-ofctl -O OpenFlow11 del-flows br0 table=0
 ovs-ofctl -O OpenFlow11 del-flows br0 table=1

echo "adding the sample flow" 
#ovs-ofctl -O OpenFlow13 add-flow br0 table=0,in_port=2,dl_type=0x0800,nw_src=10.0.0.1,actions=push_vlan:0x8100,output:1
#ovs-ofctl -O OpenFlow13 add-flow br0 table=0,in_port=2,dl_vlan=0xffff,actions=push_vlan:0x8100,output:1
ovs-ofctl -O OpenFlow11 add-flow br0 table=0,in_port=1,dl_type=0x0800,nw_src=10.0.0.1,actions=push_verify,set_verify_rule:11,set_verify_port=12,output:2
ovs-ofctl -O OpenFlow11 add-flow br0 table=0,in_port=1,dl_type=0x0800,nw_src=10.0.0.2,actions=push_verify,set_verify_rule:02,set_verify_port=1,goto_table:1
ovs-ofctl -O OpenFlow11 add-flow br0 table=1,in_port=1,dl_type=0x0800,nw_src=10.0.0.2,actions=set_verify_rule:12,output:2



echo "dumping the flow" 
 ovs-ofctl  -O OpenFlow11 dump-flows br0 

