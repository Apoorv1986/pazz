#!/bin/python

import csv
import subprocess
from datetime import datetime

prod = 0
diff = 0
exhaus = 0
FMT = '%M:%S.%f'
time = "/home/routerlab/Pazz/Experiments/Experiment5/config/generatedconfig/vagrant/config_scripts/BuggyRules/addingRules.txt"

# Parsing start times
time = open(time, "rw+")
for t in time:
    tmp = 0
    t = t.replace("\n", "") # removing line breaks
    if "Prod" in t:
        tmp = t.split('=')
        prod = tmp[1] # production start time
        continue
    if "Diff" in t:
        tmp = t.split('=')
        diff = tmp[1] # diff insertion time
        continue
    if "U-" in t:
        tmp = t.split('=')
        exhaus = tmp[1] # U- insertion time
        continue
time.close()
#print ("%s, %s, %s" % (prod, diff, exhaus))

# Parsing Detection and Localization times
with open(r'cons_output') as output:
    reader = csv.reader(output, delimiter=';')
    for line in reader:
        if '0000' in line[0]:
            header = line[0]
            detection = line[1]
            detection_delta = 0
            localization_delta = 0

            detection_delta = datetime.strptime(detection, FMT) - datetime.strptime(prod, FMT)
            detection_delta = detection_delta.total_seconds()
            loc = line[2]
            if "'" in loc:
                loc = str(line[2]).split(',')
                localization = loc[0]
                localization = localization.replace("['", "")
                localization = localization.replace("'", "")
                localization = localization.replace("]", "")
                localization_delta = 0
                localization_delta = datetime.strptime(localization, FMT) - datetime.strptime(detection, FMT)
                localization_delta = localization_delta.total_seconds()
            print("%s;%s" % ('{0:.6f}'.format(detection_delta), '{0:.6f}'.format(localization_delta) ))
