#Author = Zsolt Vagi
import os
import sys

#define which box should be edited
name = 11
while name < 45:
  if ((name == 11) or (name == 12) or (name == 13) or (name == 14) or (name == 21) or (name == 22) or (name == 23) or (name == 24) or (name == 31) or (name == 32) or (name == 33) or (name == 34) or (name == 41) or (name == 42) or (name == 43) or (name == 44)):
    #creating the file for the copied rules
    copy = open("/home/routerlab/Pazz/Experiments/Experiment_fattree/config/rawconfig/boxes/Box%s.csv" % name,"wt")
    #rule_id counter
    counter = 1
    i = 0
    while i < 100:
      f = open("/home/routerlab/Pazz/Experiments/Experiment_fattree/config/rawconfig/boxes_version3/Box%s.csv" % name,"r")
      for line in f:
        #rule_id counter for check purposes
        counter2 = 1
        while counter2 < 60:
          if 'table:0' not in line:
              if 'rule_id=%s%02d' % (name, counter2) in line:
                copy.write(line.replace('rule_id=%s%02d' % (name, counter2) ,str('rule_id=%s%02d' % (name,counter))))
                counter= counter + 1
          counter2 = counter2 + 1
      f.close()
      i = i + 1
    copy.close()
  name = name + 1
