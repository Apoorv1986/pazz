#!/bin/bash
#Author = Said Jawad Saidi
OVSDB_DIR="/usr/local/etc/openvswitch/" 
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch 
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance" 
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server" 
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl" 
ovs-vsctl --no-wait init 

echo "Deleting S8" 
ovs-vsctl --no-wait del-br s0

echo "Adding S8"
ovs-vsctl --no-wait --may-exist add-br s0 -- set bridge s0 datapath_type=netdev

echo "Adding ports" 

ovs-vsctl --no-wait --may-exist add-port s0 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s0 eth2 -- set Interface eth2 ofport_request=2
ovs-vsctl --no-wait --may-exist add-port s0 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s0 eth4 -- set Interface eth4 ofport_request=4

echo "Starting ovs-vswitch" 
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach & 

ovs-vsctl --no-wait set bridge s0 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s0 table=0

echo "adding the sample flow on S8"
ovs-ofctl -O OpenFlow11 add-flow s0 priority=100,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/32,actions=set_verify_rule:801,set_verify_port=81,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=100,table=0,in_port=2,dl_type=0x0800,nw_dst=10.1.1.6/32,actions=set_verify_rule:802,set_verify_port=82,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=100,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.7/32,actions=set_verify_rule:803,set_verify_port=81,output:3

# Fuzz Diff
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.0/27,actions=set_verify_rule:812,set_verify_port=81,output:3

# Fuzz U-
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=8.1.1.1/8,actions=set_verify_rule:820,set_verify_port=81,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=16.1.1.1/8,actions=set_verify_rule:821,set_verify_port=81,output:3

echo "dumping the flow from S8"
ovs-ofctl -O OpenFlow11 dump-flows s0
sleep 8s
