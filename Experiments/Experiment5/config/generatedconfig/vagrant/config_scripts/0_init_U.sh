#!/bin/bash
#Author = Seifeddine Fathalli

#echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s0 table=0


#echo "adding the sample flow on s0"
# Production traffic different paths
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/32,actions=push_verify,set_verify_rule:1,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.2/32,actions=push_verify,set_verify_rule:2,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.3/32,actions=push_verify,set_verify_rule:3,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.4/32,actions=push_verify,set_verify_rule:4,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.5/32,actions=push_verify,set_verify_rule:5,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.6/32,actions=push_verify,set_verify_rule:6,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.7/32,actions=push_verify,set_verify_rule:7,set_verify_port=1,output:4
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.8/32,actions=push_verify,set_verify_rule:8,set_verify_port=1,output:3

# Production same paths, different rules
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.10/32,actions=push_verify,set_verify_rule:9,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.15/32,actions=push_verify,set_verify_rule:10,set_verify_port=1,output:4

# Fuzz U-
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=8.1.1.1/8,actions=push_verify,set_verify_rule:20,set_verify_port=1,output:3
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=16.1.1.1/8,actions=push_verify,set_verify_rule:21,set_verify_port=1,output:3

#echo "dumping the flow from s0"
ovs-ofctl -O OpenFlow11 dump-flows s0
