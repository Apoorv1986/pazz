#!/bin/bash

#echo "starting packet_gen ..."
vagrant ssh gen20 -c 'cd /vagrant/config_scripts; sudo python packet_gen.py' &

time=$(date +"%M:%S.%6N")
echo "Prod=$time" > /home/routerlab/Pazz/Experiments/Experiment5/config/generatedconfig/vagrant/config_scripts/BuggyRules/addingRules.txt

#echo "tcpreplay prod." prod_1-15.pcap
sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=100000 --pps-multi=100000 --intf1=eth1 /vagrant/config_scripts/pcaps/prod_1-15.pcap &

#echo "tcpreplay fuzz"
#vagrant ssh gen20 -c 'cd /vagrant/config_scripts; sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz_exhaust_first.pcap' &

sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz_diff.pcap &
sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz_U.pcap &
#sleep 5s

#echo "s0 inserting Diff Bugs"
#vagrant ssh s0 -c 'cd /vagrant/config_scripts; sudo -E ./0_diff_bugs.sh' &

#sleep 5s

#echo "s0 inserting U- Bugs"
#vagrant ssh s0 -c 'cd /vagrant/config_scripts; sudo -E ./0_U_bugs.sh' &


