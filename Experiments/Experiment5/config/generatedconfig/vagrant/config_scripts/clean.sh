#!/bin/bash

#echo "Cleaning ..."
vagrant ssh gen20 -c 'sudo kill -9  $(ps aux | grep '\''[t]cpreplay'\'' | awk '\''{print $2}'\'')'
vagrant ssh gen20 -c 'sudo kill -9 $(ps aux | grep '\''[s]udo tcpreplay'\'' | awk '\''{print $2}'\'')'
vagrant ssh gen20 -c 'sudo kill -9  $(ps aux | grep '\''[p]ython'\'' | awk '\''{print $2}'\'')'
vagrant ssh gen20 -c 'sudo kill -9 $(ps aux | grep '\''[s]udo python'\'' | awk '\''{print $2}'\'')'

#echo "restart network service ..."
#vagrant ssh gen20 -c 'sudo ifdown --exclude=lo -a && sudo ifup --exclude=lo -a'
#sleep 5s

#echo "s0 initiate rules ..."
#vagrant ssh s0 -c 'cd /vagrant/config_scripts; sudo -E ./0_init_diff.sh; sleep 8'
#vagrant ssh s0 -c 'cd /vagrant/config_scripts; sudo -E ./0_init_U.sh; sleep 8'
#vagrant ssh s0 -c 'cd /vagrant/config_scripts; sudo -E ./0_init.sh; sleep 8'

#vagrant ssh s1 -c 'cd /vagrant/config_scripts; sudo -E ./1.sh'
#vagrant ssh s2 -c 'cd /vagrant/config_scripts; sudo -E ./2.sh'
#vagrant ssh s3 -c 'cd /vagrant/config_scripts; sudo -E ./3.sh'
#vagrant ssh s4 -c 'cd /vagrant/config_scripts; sudo -E ./4.sh'
#vagrant ssh s5 -c 'cd /vagrant/config_scripts; sudo -E ./5.sh'
#vagrant ssh s6 -c 'cd /vagrant/config_scripts; sudo -E ./6.sh'
#vagrant ssh s7 -c 'cd /vagrant/config_scripts; sudo -E ./7.sh'
#vagrant ssh s8 -c 'cd /vagrant/config_scripts; sudo -E ./8.sh'
#vagrant ssh s9 -c 'cd /vagrant/config_scripts; sudo -E ./9.sh'
#vagrant ssh s10 -c 'cd /vagrant/config_scripts; sudo -E ./10.sh'
#vagrant ssh s11 -c 'cd /vagrant/config_scripts; sudo -E ./11.sh'
#vagrant ssh s12 -c 'cd /vagrant/config_scripts; sudo -E ./12.sh'
#vagrant ssh s13 -c 'cd /vagrant/config_scripts; sudo -E ./13.sh'
#vagrant ssh s14 -c 'cd /vagrant/config_scripts; sudo -E ./14.sh'
#vagrant ssh s15 -c 'cd /vagrant/config_scripts; sudo -E ./15.sh'
#vagrant ssh s30 -c 'cd /vagrant/config_scripts; sudo -E ./30.sh'
