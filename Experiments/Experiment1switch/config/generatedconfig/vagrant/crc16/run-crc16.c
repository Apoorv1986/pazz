#include "crc16.c"
#include <stdio.h>

int main(int argc, char *argv[]){

unsigned short value;
unsigned short basis;
sscanf(argv[1], "%hu", &value);
sscanf(argv[2], "%hu", &basis);

const void *buf = &value;
const unsigned short crc = crc16(buf,2,basis);
return crc;
}

