#!/bin/bash

#echo "starting packet_gen ..."
#vagrant ssh gen5 -c 'cd /vagrant/config_scripts; sudo python packet_gen.py' &

#sleep 5s

#echo "tcpreplay diff."
#sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=500 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/diff.pcap &

#echo "tcpreplay U-s15"
#sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=500 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/U-s15.pcap &

#echo "tcpreplay fuzz"
#vagrant ssh gen5 -c 'cd /vagrant/config_scripts; sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz.pcap' &

#sleep 5s

#echo "s0 inserting Bugs"
#vagrant ssh s0 -c 'cd /vagrant/config_scripts; sudo -E ./0_bugs.sh' &

echo "tcpreplay prod."
vagrant ssh gen -c 'cd /vagrant/config_scripts; sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000000 --intf1=eth1 /vagrant/config_scripts/pcaps/prod.pcap' &

