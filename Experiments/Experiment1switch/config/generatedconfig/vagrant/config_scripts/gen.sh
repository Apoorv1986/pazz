#!/bin/bash
export https_proxy=https://:@proxy.routerlab:8080
export http_proxy=http://:@proxy.routerlab:8080

sudo timedatectl set-timezone Europe/Berlin

sudo route add -net 10.1.1.0 netmask 255.255.255.0 gw 172.16.21.10  dev eth1 

sudo apt-get update
sudo apt-get install tcpreplay
sudo apt-get -y install gcc python-dev libffi-dev libssl-dev libxml2-dev libxslt1-dev zlib1g-dev libyaml-cpp-dev python-pbr

pip install psutil
pip install ryu

#sudo python /vagrant/config_scripts/packet_gen.py > /vagrant/Fuzz_Output/Errors.log &
#sudo tcpreplay --loop=1 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/packet.pcap &
