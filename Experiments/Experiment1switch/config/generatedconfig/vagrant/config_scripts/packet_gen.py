from ryu.lib import addrconv
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet, ipv4, tcp, vlan
import struct 
import socket
import psutil

address = ('192.168.20.20',9955)

def send(header):
	tcp_header = tcp.tcp()
	ip_header = ipv4.ipv4(proto=6)
	packet_str = "!6s6sH"
	pp =packet.Packet()
	ipp = ipv4.ipv4(proto=6, src='172.16.21.20', dst=header)
	pp.add_protocol(ipp)
	pp.add_protocol(tcp_header)
	pp.serialize()
	pp.data.extend(b'\x00' *(110-len(pp.data)))
	pp.serialize()
	h = struct.pack(packet_str, addrconv.mac.text_to_bin("11:22:33:44:55:66"), addrconv.mac.text_to_bin("77:88:99:00:11:22") ,2048) + pp.data
	try:
		s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
		s.bind(("eth1", 0))
		for i in range(0,5):#sending the header 5 times for sampling
			s.send(h)
	except socket.error as e: 
		print(e)


def main():

    try:
        for proc in psutil.process_iter():
            for pcon in proc.connections(kind='all'):
                #print pcon
                if pcon.laddr and (pcon.laddr[1] == 9955):
                    proc.terminate()
    except:
        print 'failed to perform clean up, make sure that you execute the experiment with root privileges'

    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind(address)
    serversocket.listen(1)
    while True:
        connection, client_address = serversocket.accept()
        try:
            buf = connection.recv(32)
            print("received: %s" % buf)
            send(buf) 
        finally:
            connection.close()

if __name__ =="__main__":
	main()
