#!/bin/bash
#Author: Apoorv Shukla
sudo timedatectl set-timezone Europe/Berlin

OVSDB_DIR="/usr/local/etc/openvswitch/" 
rm -rf $OVSDB_DIR
if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch 
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance" 
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server" 
#ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate$
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file

echo "initializing ovs-vsctl" 
ovs-vsctl --no-wait init 

echo "Deleting s6" 
ovs-vsctl --no-wait del-br s6

echo "Adding s6"
ovs-vsctl --no-wait --may-exist add-br s6 -- set bridge s6 datapath_type=netdev

echo "Adding ports" 

ovs-vsctl --no-wait --may-exist add-port s6 eth1 -- set Interface eth1 ofport_request=1
echo "Starting ovs-vswitch" 
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach & 
sleep 10
ovs-vsctl --no-wait set bridge s6 protocols=OpenFlow11
ovs-ofctl -O OpenFlow11 del-flows s6 table=0

ovs-vsctl -- --id=@sflow create sflow agent=eth2 target=\"192.168.20.1:6343\" header=128 sampling=100 polling=1 -- set bridge s6 sflow=@sflow
ovs-ofctl -O OpenFlow11 add-flow s6 priority=10,table=0,in_port=1,actions=DROP
 

#echo "Starting ovs-vswitch" 
#export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
#ovs-vswitchd unix:$DB_SOCK --pidfile --detach & 

#ovs-vsctl --no-wait set bridge s0 protocols=OpenFlow11

sleep 8s
