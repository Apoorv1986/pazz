#!/bin/bash
#Author = Said Jawad Saidi
sudo apt-get install iptraf
OVSDB_DIR="/usr/local/etc/openvswitch/" 
rm -rf $OVSDB_DIR

if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch 
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance" 
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server" 
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl" 
ovs-vsctl --no-wait init 

echo "Deleting S4" 
ovs-vsctl --no-wait del-br s4

echo "Adding S4"
ovs-vsctl --no-wait --may-exist add-br s4 -- set bridge s4 datapath_type=netdev

echo "Adding ports" 

ovs-vsctl --no-wait --may-exist add-port s4 eth1 -- set Interface eth1 ofport_request=1
ovs-vsctl --no-wait --may-exist add-port s4 eth3 -- set Interface eth3 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s4 eth2 -- set Interface eth2 ofport_request=4

echo "Starting ovs-vswitch" 
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach & 

ovs-vsctl --no-wait set bridge s4 protocols=OpenFlow11

sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s4 table=0

echo "adding the sample flow on S4"
ovs-ofctl -O OpenFlow11 add-flow s4 priority=8,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/28,actions=push_verify,set_verify_rule:401,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=74.1.1.1/28,actions=push_verify,set_verify_rule:402,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=9,table=0,in_port=1,dl_type=0x0800,nw_dst=74.1.1.1/27,actions=push_verify,set_verify_rule:403,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=8,table=0,in_port=1,dl_type=0x0800,nw_dst=74.1.1.1/26,actions=push_verify,set_verify_rule:404,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=9,table=0,in_port=1,dl_type=0x0800,nw_dst=138.1.1.1/28,actions=push_verify,set_verify_rule:405,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=9,table=0,in_port=1,dl_type=0x0800,nw_dst=138.1.1.1/27,actions=push_verify,set_verify_rule:406,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=8,table=0,in_port=1,dl_type=0x0800,nw_dst=138.1.1.1/26,actions=drop

#ovs-ofctl -O OpenFlow11 add-flow s0 priority=7,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/27,actions=push_verify,set_verify_rule:407,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=6,table=0,in_port=1,dl_type=0x0800,nw_dst=10.1.1.1/26,actions=push_verify,set_verify_rule:408,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=0.1.1.1/8,actions=push_verify,set_verify_rule:409,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=2.1.1.1/8,actions=push_verify,set_verify_rule:410,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=4.1.1.1/8,actions=push_verify,set_verify_rule:411,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=8.1.1.1/8,actions=push_verify,set_verify_rule:412,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=16.1.1.1/8,actions=push_verify,set_verify_rule:413,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=32.1.1.1/8,actions=push_verify,set_verify_rule:414,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=64.1.1.1/8,actions=push_verify,set_verify_rule:415,set_verify_port=41,output:3
#ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=1,dl_type=0x0800,nw_dst=128.1.1.1/8,actions=push_verify,set_verify_rule:416,set_verify_port=41,output:3

echo "dumping the flow from S4"
ovs-ofctl -O OpenFlow11 dump-flows s4
sleep 8s
