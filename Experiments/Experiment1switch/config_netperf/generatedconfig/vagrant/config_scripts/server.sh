#!/bin/bash
export https_proxy=https://:@proxy.routerlab:8080
export http_proxy=http://:@proxy.routerlab:8080

sudo timedatectl set-timezone Europe/Berlin

sudo route add -net 172.16.25.0 netmask 255.255.255.0 gw 172.16.21.10  dev eth1 

sudo apt-get update
sudo apt-get install iptraf
sudo apt-get install iperf

