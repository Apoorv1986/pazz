#!/bin/bash

#echo "iptraf gen5"
#vagrant ssh gen5 -c 'sudo -E iptraf -g -d eth0 &; sudo iptraf -g -d eth1' &

echo "tcpreplay prod."
vagrant ssh gen5 -c 'cd /vagrant/config_scripts; sudo tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000000 --pps-multi=1000000 --intf1=eth1 /vagrant/config_scripts/pcaps/prod.pcap' &
