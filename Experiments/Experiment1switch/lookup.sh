#!/bin/bash
grep -m 1 '00001010000000010000000100000001' consistency_testing_output
grep -m 1 '00001010000000010000000100001010' consistency_testing_output
grep -m 1 '00001010000000010000000100001111' consistency_testing_output

grep -m 1 '00001010000000010000000100010000' consistency_testing_output
grep -m 1 '00001010000000010000000100010001' consistency_testing_output
grep -m 1 '00001010000000010000000100011110' consistency_testing_output
grep -m 1 '00001010000000010000000100011111' consistency_testing_output

grep -m 1 '00010000000000010000000100000001' consistency_testing_output
grep -m 1 '01000000000000010000000100000001' consistency_testing_output
grep -m 1 '10000000000000010000000100000001' consistency_testing_output
