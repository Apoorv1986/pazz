#!/bin/sh
OVSDB_DIR="/usr/local/etc/openvswitch/" 
rm -rf $OVSDB_DIR
if [ ! -d $OVSDB_DIR ]; then
echo "creating directory"
mkdir -p /usr/local/etc/openvswitch 
cd /home/vagrant/ovs
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
cd /home/vagrant/
fi

echo "Killing the OVSDB-server instance" 
pkill ovsdb-server
pkill ovs-vswitchd


echo "starting a new instance of ovsdb-server" 
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock     --remote=db:Open_vSwitch,Open_vSwitch,manager_options     --private-key=db:Open_vSwitch,SSL,private_key     --certificate=db:Open_vSwitch,SSL,certificate     --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert     --pidfile --detach --log-file


echo "initializing ovs-vsctl" 
ovs-vsctl --no-wait init 

echo "Deleting S0" 
ovs-vsctl --no-wait del-br s0

echo "Adding S0"
ovs-vsctl --no-wait --may-exist add-br s0 -- set bridge s0 datapath_type=netdev

echo "Adding ports" 

ovs-vsctl --no-wait --may-exist add-port s0 eth1 -- set Interface eth1 ofport_request=3
ovs-vsctl --no-wait --may-exist add-port s0 eth2 -- set Interface eth2 ofport_request=1

echo "Starting ovs-vswitch" 
export DB_SOCK=/usr/local/var/run/openvswitch/db.sock
ovs-vswitchd unix:$DB_SOCK --pidfile --detach & 

ovs-vsctl --no-wait set bridge s0 protocols=OpenFlow11
sleep 10

echo "deleting old openflow rules"
ovs-ofctl -O OpenFlow11 del-flows s0 table=0

echo "adding the sample flow on S0"  
# Production traffic different paths
ovs-ofctl -O OpenFlow11 add-flow s0 priority=100,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.1/32,actions=set_verify_rule:101,set_verify_port=13,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=100,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.2/32,actions=set_verify_rule:102,set_verify_port=13,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=100,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.3/32,actions=set_verify_rule:103,set_verify_port=13,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=100,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.4/32,actions=set_verify_rule:104,set_verify_port=13,output:1

# Production same paths, different rules
ovs-ofctl -O OpenFlow11 add-flow s0 priority=100,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.10/32,actions=set_verify_rule:106,set_verify_port=13,output:1

# Fuzz Difference
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=10.1.1.0/27,actions=set_verify_rule:112,set_verify_port=13,output:1

# Fuzz U-
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=8.1.1.1/8,actions=set_verify_rule:120,set_verify_port=13,output:1
ovs-ofctl -O OpenFlow11 add-flow s0 priority=10,table=0,in_port=3,dl_type=0x0800,nw_dst=16.1.1.1/8,actions=set_verify_rule:121,set_verify_port=13,output:1

echo "dumping the flow from S0"
ovs-ofctl -O OpenFlow11 dump-flows s0
sleep 8s
