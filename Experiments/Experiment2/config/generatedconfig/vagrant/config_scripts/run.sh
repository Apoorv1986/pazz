#!/bin/bash

#echo "starting packet_gen ..."
#sudo python packet_gen.py &

time=$(date +"%M:%S.%6N")
echo "Prod=$time" > /vagrant/config_scripts/BuggyRules/addingRules.txt

#echo "tcpreplay prod." prod_1-15.pcap
sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=100000 --pps-multi=100000 --intf1=eth1 /vagrant/config_scripts/pcaps/prod_1-15.pcap &

#echo "tcpreplay fuzz"
#sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz_exhaust_first.pcap &

#sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz_diff.pcap &
#sudo  tcpreplay --loop=0 --enable-file-cache --preload-pcap --pps=1000 --pps-multi=1000 --intf1=eth1 /vagrant/config_scripts/pcaps/fuzz_U.pcap &

