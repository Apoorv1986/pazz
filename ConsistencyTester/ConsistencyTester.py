import Queue
import SocketServer
import struct
import threading
import timeit
import datetime
import time

# from SampleReceiver.FlowCollector import FlowC
from searchservice import pathServiceClient
from SampleReceiver import FlowCollector
from lib.crchash import crchash
from netaddr.strategy.ipv4 import *
from bloom import bloom

AGENTS = {"192.168.20.62": {3: 43}}
VERIFY_ETHER_TYPE = 2080


def extract_header(payload):
    if len(payload) >= 42:
        offset = 12
        verify_header_ether_type = struct.unpack_from("!H", payload, offset=offset)[0]
        # print verify_header_ether_type
        if verify_header_ether_type != VERIFY_ETHER_TYPE:
            return None
        offset += 2
        verify_rule, verify_port, ether_type, version, \
        tos, total_length, identification, flags, ttl, proto, csum, src, dst \
            = struct.unpack_from('!HLHBBHHHBBH4s4s', payload, offset=offset)
        dst_ip = int_to_bits(packed_to_int(dst)).replace(".", "")
        print int_to_str(packed_to_int(dst)), verify_rule, verify_port
        return dst_ip, verify_rule, verify_port
    return None, None, None


def test_consistency(in_port, verify_rule, verify_port, expected_paths):
    expected_rules = expected_paths[1:len(expected_paths) / 2]
    expected_ports = expected_paths[len(expected_paths) / 2 + 1:]
    print expected_rules, "E R"
    print expected_ports, "E P"
    expected_rule_hashes = []
    for rules in reversed(expected_rules):
        hash = 1
        print rules
        for r in reversed((rules[1:len(rules) - 1]).strip().split(",")):
            hash = crchash(int(r), hash)
        expected_rule_hashes.append(hash)
    print expected_rule_hashes, "Expected Rule Hashes"
    if verify_rule in expected_rule_hashes:
        return True
    expected_port_bloom = []
    for ports in reversed(expected_ports):
        Bloom = 1
        print ports, "Ports"
        for p in reversed((ports[1:len(ports) - 1]).strip().split(",")):
            Bloom = bloom(Bloom, int(p))
        expected_port_bloom.append(Bloom)
        diff = Bloom & verify_port
        if diff == Bloom:
            print "This round has no problem"
        else:
            EB = bin(Bloom)[2:].zfill(32)
            AB = bin(verify_port)[2:].zfill(32)
            Diff = bin(diff)[2:].zfill(32)
            print "This %i round has problems since EB = %s is not equal to AND = %s", ports, str(EB), str(Diff)
       # print "The AND is %s", str(Diff)
       # bitPosition = Diff.bit_length()-1
       # print "Bit position is %s", str(bitPosition)
    print AB, EB, Diff, "Expected Port Bloom is", expected_port_bloom
    return False


def main():
    global recvqueue
    recvqueue = Queue.Queue(maxsize=3000)

    print("Consistency Tester Starting")

    FlowCollector.recvqueue = recvqueue
    # print "starting flow collector thread"
    SocketServer.UDPServer.allow_reuse_address = True
    flow_collector = SocketServer.UDPServer(("0.0.0.0", 6343), FlowCollector.FlowC)
    flow_collector_thread = threading.Thread(target=flow_collector.serve_forever)
    flow_collector_thread.setDaemon(True)
    flow_collector_thread.start()
    path_service_client = pathServiceClient.PathServiceClient()
    path_service_client.connect()
    print "Connected to control plane component"
    outputfile = open("out.txt", "a+")
    while True:
        print "waiting for incoming packets"
#        global timer
        timer = timeit.default_timer()
        time = datetime.datetime.now()
        outputfile.write("<<< %s >>>\n"%(str(time)))
        sample = recvqueue.get()
        print int_to_str(sample['agent_address'])

        for sample in sample['samples']:
            print sample
            out_port = sample['output']
            in_port = sample['input']
            print 'outport is:', out_port, ' inport is', in_port

            #print 'sample is ', sample, 'outport ' , port
            for flow in sample['flows']:
                if flow is not None and flow['payload'] is not None:
                    header, verify_rule, verify_port = extract_header(flow['payload'])
                    print header, verify_rule, verify_port
                    print flow
                    if header is not None and in_port:
                        expected_paths = path_service_client.find_path(AGENTS["192.168.20.62"][in_port], header)
                        print AGENTS["192.168.20.62"][in_port]
                        consistent = test_consistency(AGENTS["192.168.20.62"][in_port], verify_rule, verify_port,
                                                      expected_paths)
                        if consistent:
                            print "consistent"
                        if not consistent:
                            print 'inconsistency detected'
                            time = datetime.datetime.now()
                            timer = timeit.default_timer() - timer
                            outputfile.write("<<<%s, %s, %s, %s>>>\n"%(str(time), str(timer), header, AGENTS["192.168.20.62"][in_port]))
                            outputfile.flush()


def _shadow_main():
    path_service_client = pathServiceClient.PathServiceClient()
    path_service_client.connect()
    result = path_service_client.find_path(43, "00001010000000010000000100000001")
    print result
    #test_consistency((1, "a"), result)

def prepare_header():
    headers = []
    for i in range(100):
        header = "00001010000000010000000100000001"
        headers.append(header)
    return headers


def _shadow_main2():
    path_service_client = pathServiceClient.PathServiceClient()
    path_service_client.connect()
    headers = prepare_header()
    exit_port = 43

    print "Shadow_main2"
    for h in headers:
        startcp = timeit.default_timer()
        expected_paths = path_service_client.find_path(exit_port, h)
        endcp = timeit.default_timer() - startcp 
        start = timeit.default_timer()
        consistent = test_consistency(exit_port, 1, 2, expected_paths)
        if consistent:
            print "consistent"
        if not consistent:
            print 'inconsistency detected'
    duration = timeit.default_timer() - start
    total = endcp + duration
    outputfile = open("cp_ct_performance.txt", "a+")

    outputfile.write("<<<%s, %s, %s, %s, %s, %s>>> \n" % (str(endcp), str(endcp), str(duration), duration/len(headers), str(total), total/len(headers)))
    outputfile.flush()
    outputfile.close()

print "Test Print"
if __name__ == "__main__":
    print "Consistency Tester Starting"
#    main()
    #_shadow_main()
    time.sleep(20)
    _shadow_main2()
#main()
