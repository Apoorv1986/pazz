#include <Python.h>
#include "bloom.h"



static PyObject *
bloom_wrapper(PyObject *self, PyObject *args)
{

    uint32_t *value = malloc(4);
	uint32_t *filter = malloc(4);
    if (!PyArg_ParseTuple(args, "kk", filter, value))
        return NULL;
   bloom_add(filter,*value);
 	uint32_t res = *filter;
  	free(filter);
	free(value);
    return Py_BuildValue("k", res);
}

static PyMethodDef BloomMethods[] = {
    
    {"bloom",  bloom_wrapper, METH_VARARGS,
     "Calculate bloom filter."},
    
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

DL_EXPORT(void) initbloom(void)
{

    Py_InitModule("bloom", BloomMethods);

}
