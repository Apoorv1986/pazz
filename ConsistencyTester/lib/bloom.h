#ifndef _BLOOM_H
#define _BLOOM_H
#include <stdint.h>

void bloom_free(uint32_t filter);
void bloom_add(uint32_t *filter, uint32_t item);

#endif _BLOOM_H