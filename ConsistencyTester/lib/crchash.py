import crcmod.predefined
from binascii import unhexlify
import struct

def crchash(value, basis):
    crc16 = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0x0000, xorOut=0x0000)

    value = struct.pack("!H", value )
    valuex = ':'.join(x.encode('hex') for x in value)
    print ("----- crc calculation value: %s" % valuex)

    hash = crc16(value)
    #format(hash, 'x')
    return int(hash)^basis
