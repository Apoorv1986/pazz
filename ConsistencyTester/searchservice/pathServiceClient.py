import PathService,threading
from thrift.protocol import TBinaryProtocol
from thrift.transport import TSocket


class PathServiceClient():
    def __init__(self, hostname='localhost', port=9090):
        self.hostname = hostname
        self.port = port
        self.transport = TSocket.TSocket(hostname, port)
        self.protocol = TBinaryProtocol.TBinaryProtocol(self.transport)
        self.client = PathService.Client(self.protocol)

    def connect(self):
        while 1:
            try:
                self.transport.open()
                #print("Path Service Client is connected :)")
            except:
                #print("Could not open the connection to PathServer!")
                continue #exit(-10)
            break
  
    def close(self):
        try:
            self.transport.close()
        except:
            print("Could not close the connection to the PathServer")

    def find_path(self, egress_port, packet_header):
        return self.client.findPaths(egress_port, packet_header)

