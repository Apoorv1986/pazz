import Queue
import SocketServer
import struct
import threading
import timeit
import datetime
import time
import random
import multiprocessing
import subprocess

from searchservice import pathServiceClient
from SampleReceiver import FlowCollector
from netaddr.strategy.ipv4 import *
from bloom import bloom
from threading import Thread
from multiprocessing import Process

AGENTS = {"192.168.20.30": {3: 243}}
VERIFY_ETHER_TYPE = 2080
HEADERS_LIST = []

consistency_output = open("/home/routerlab/Pazz/Experiments/Experiment_fattree/consistency_testing_output", "a+")
def extract_header(payload):
    if len(payload) >= 42:
        offset = 12
        verify_header_ether_type = struct.unpack_from("!H", payload, offset=offset)[0]
        if verify_header_ether_type != VERIFY_ETHER_TYPE:
            return None
        offset += 2
        verify_rule, verify_port, ether_type, version, \
        tos, total_length, identification, flags, ttl, proto, csum, src, dst \
            = struct.unpack_from('!HLHBBHHHBBH4s4s', payload, offset=offset)
        dst_ip = int_to_bits(packed_to_int(dst)).replace(".", "")
        return dst_ip, verify_rule, verify_port
    return None, None, None

def crc16(rule, basis):
    cmd = "./run-crc16"
    var1 = "%s" % rule
    var2 = "%s" % basis
    p = subprocess.Popen([cmd, var1, var2], stdout=subprocess.PIPE)
    basis, err = p.communicate()
    return basis

def test_consistency():
    global recvqueue
    recvqueue = Queue.Queue(maxsize=0)
    FlowCollector.recvqueue = recvqueue
    SocketServer.UDPServer.allow_reuse_address = True

    flow_collector = SocketServer.UDPServer(("0.0.0.0", 6343), FlowCollector.FlowC)
    flow_collector_thread = threading.Thread(target=flow_collector.serve_forever)
    flow_collector_thread.setDaemon(True)
    flow_collector_thread.start()

    path_service_client = pathServiceClient.PathServiceClient()
    path_service_client.connect()

    while True:
        #consistency_output.write("Sample Pull timestamp %s >>>\n" % str(time))
        sample = recvqueue.get()
        for sample in sample['samples']:
            in_port = sample['input']
            for flow in sample['flows']:
                if flow is not None and flow['payload'] is not None:
                    header, verify_rule, verify_port = extract_header(flow['payload'])
                    #consistency_output.write("\nHeader: %s, verify_rule: %s, verify_port: %s\n" % (header, verify_rule, verify_port))
                    if header is not None and in_port:
                        expected_paths = path_service_client.find_path(AGENTS["192.168.20.30"][in_port], header)
                        expected_rules = expected_paths[1:len(expected_paths) / 2]
                        expected_ports = expected_paths[len(expected_paths) / 2 + 1:]
                        #consistency_output.write("ER: %s, EP: %s\n" % (expected_rules, expected_ports))
                        expected_rule_hashes = []

                        #timer = timeit.default_timer()
                        localization_time = []
                        detection_time = 0
                        for rules in reversed(expected_rules):
                            hash = 1
                            for r in reversed((rules[1:len(rules) - 1]).strip().split(",")):
                                hash = crc16(r, hash)
                                expected_rule_hashes.append(int(hash))
                # =================== Detection ==========================
                        if verify_rule in expected_rule_hashes:
                            continue
                        else:
                            detection_time = datetime.datetime.now().strftime('%M:%S.%f')
                            #localization_time = []
                            #expected_port_bloom = []
                            #EB = None
                            #AB = None
                            #Diff = None
                # =================== Localization =======================
                            #timer = timeit.default_timer()
                            #consistency_output.write("ER: %s, EP: %s\n" % (expected_rules, expected_ports))

                            for ports in reversed(expected_ports):
                                Bloom = 1
                                localization = 0
                                #round = 0
                                for p in reversed((ports[1:len(ports) - 1]).strip().split(",")):
                                    Bloom = bloom(Bloom, int(p))
                                    #expected_port_bloom.append(Bloom)
                                    diff = Bloom & verify_port
                                    #consistency_output.write("Port: %s\n" % p)
                                    #consistency_output.write("Bloom: %s\n" % Bloom)
                                    #consistency_output.write("diff: %s\n" % diff)
                                    if diff == Bloom:
                                        continue
                                    else:
                                        #localization_time.append( (int(p), timeit.default_timer() - timer) )
                                        localization = datetime.datetime.now().strftime('%M:%S.%f')
                                        #localization = str(localization.minute) + ':' + str(localization.second) + '.' + str(localization.microsecond)
                                        localization_time.append(localization)
                                        break
                                        #consistency_output.write("%s; %s; %s; %s\n" % (header, expected_ports[::-1], detection_time, localization_time) )
                            consistency_output.write("%s;%s;%s\n" % (header, detection_time, localization_time) )
					#EB = bin(Bloom)[2:].zfill(32)
                                        #AB = bin(verify_port)[2:].zfill(32)
                                        #Diff = bin(diff)[2:].zfill(32)
                                    #round = round + 1



if __name__ == "__main__":
    test_consistency()
    consistency_output.close()
