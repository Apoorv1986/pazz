import argparse, os

number_of_nodes = 4
BASE_IMAGE = "veritag"
BOX_MULTIPLIER = 10


class Switch(object):
    def __init__(self, switch_id):
        self.switch_id = switch_id
        self.name = "s%s" % self.switch_id
        self.interfaces = {}
        self.memory_size = 1024
        self.cpu_count = 1
        self.is_connected_to_collector = False
        self.init_script = "sudo -E /vagrant/config_scripts/%s.sh" % self.switch_id
        self.gen_interfaces = {}

    def add_interface(self, local_interface, remote_interface):
        if local_interface < remote_interface:
            self.interfaces[local_interface] = remote_interface
        else:
            self.interfaces[remote_interface] = local_interface

    def customize_memory(self, memory_size):
        self.memory_size = memory_size

    def customize_cpu(self, cpu_count):
        self.cpu_count = cpu_count

    def custom_init_script(self, script):
        self.init_script = script

    def add_rcv_interface(self, interface, rcv_id):
        self.rcv_interfaces[interface] = rcv_id

    def add_gen_interface(self, interface, gen_id):
        self.gen_interfaces[interface] = gen_id

    def virtual_box_string(self):
        virtual_box_string = "\t\t%s.vm.provider \"virtualbox\" do |virtualbox|\n" % self.name
        if self.memory_size:
            virtual_box_string += "\t\t\tvirtualbox.memory = \"%s\"\n" % self.memory_size
        if self.cpu_count:
            virtual_box_string += "\t\t\tvirtualbox.cpus = \"%s\"\n" % self.cpu_count
        for local_interface in range(self.interfaces.__len__() + self.gen_interfaces.__len__()):
            virtual_box_string += "\t\t\tvirtualbox.customize [\"modifyvm\", :id," \
                                  "\"--nicpromisc%s\", \"allow-all\"]\n" % str(local_interface + 2)
        virtual_box_string += "\t\tend\n"
        return virtual_box_string

    def __get_local_iface(self,interface_tuple):
        """
        Find the portID which belongs to current switch
        """
        if (interface_tuple[0]/BOX_MULTIPLIER) == self.switch_id:
            return interface_tuple[0]
        return interface_tuple[1]

    def __sort_interfaces(self):
        """
        bubble sort the generator-connected and switch-connected interfaces according to their ID numbers.
        """
        sorted_interfaces = [(k,v)for k,v in self.interfaces.iteritems()]
        sorted_interfaces+=[(k,v) for k,v in self.gen_interfaces.iteritems()]
        for i in range(len(sorted_interfaces)):
            for j in range(len(sorted_interfaces)-1):
                first_intf = self.__get_local_iface(sorted_interfaces[j])
                second_intf = self.__get_local_iface(sorted_interfaces[j+1])
                if first_intf > second_intf:
                    sorted_interfaces[j], sorted_interfaces[j+1] = sorted_interfaces[j+1], sorted_interfaces[j]
        return sorted_interfaces

    def to_string(self):
        config_script = "\tconfig.vm.define \"%s\" do |%s|\n\t\t%s.vm.box = \"%s\"\n" % (
            self.name, self.name, self.name, BASE_IMAGE)
        network_script = ""
        sorted_interfaces = self.__sort_interfaces()
        for (local_interface, remote_interface), int_index in zip(sorted_interfaces,
                                                                  range(len(sorted_interfaces))):
            interface_ip = "%s" % (self.switch_id * BOX_MULTIPLIER + int_index + 1)
            if local_interface in self.interfaces.keys() + self.interfaces.values():
                network_script += "\t\t%s.vm.network \"private_network\"," \
                                  "ip: \"172.16.20.%s\", netmask: \"255.255.255.0\"," \
                                  "virtualbox__intnet: \"%s-%s\"\n" % (
                                      self.name, interface_ip, local_interface, remote_interface)
            if local_interface in self.gen_interfaces:
                network_script += "\t\t%s.vm.network \"private_network\"," \
                             "ip: \"172.16.21.%s\", netmask: \"255.255.255.0\"," \
                             "virtualbox__intnet: \"%s-%s\"\n" % (
                                 self.name, interface_ip, remote_interface, local_interface)
        config_script += network_script
        if self.is_connected_to_collector:
            rcv_script = ""
            interface_ip = "%s" % (min(254, self.switch_id * BOX_MULTIPLIER + 2))
            rcv_script += "\t\t%s.vm.network \"private_network\"," \
                          "ip: \"192.168.20.%s\", netmask: \"255.255.255.0\"\n"% (self.name, interface_ip )
            config_script += rcv_script
        config_script += self.virtual_box_string()
        config_script += "\t\t%s.vm.provision \"shell\", inline:\"%s\"\n" % (self.name, self.init_script)
        config_script += "\tend\n"
        # print config_script
        return config_script


class Generator(Switch):
    def __init__(self, gen_id):
        Switch.__init__(self, gen_id)
        self.name = "gen%s" % (gen_id)

    def add_interface(self, remote_interface, local_interface):
        self.interfaces[local_interface] = remote_interface

    def to_string(self):
        config_script = "\tconfig.vm.define \"%s\" do |%s|\n\t\t%s.vm.box = \"%s\"\n" % (
            self.name, self.name, self.name, BASE_IMAGE)
        network_script = ""
        sorted_interfaces = self.interfaces.keys()
        sorted_interfaces.sort()
        for local_interface, int_index in zip(sorted_interfaces,range(self.interfaces.__len__())):
            remote_interface =self.interfaces[local_interface]
            interface_ip = "%s" % (self.switch_id * BOX_MULTIPLIER + int_index + 1)
            network_script += "\t\t%s.vm.network \"private_network\"," \
                              "ip: \"172.16.21.%s\", netmask: \"255.255.255.0\"," \
                              "virtualbox__intnet: \"gen%s-%s-%s\"\n" % (
                                  self.name, interface_ip, self.switch_id, local_interface, remote_interface)
        config_script += network_script
        config_script += self.virtual_box_string()
        config_script += "\t\t%s.vm.provision \"shell\", inline:\"%s\"\n" % (self.name, self.init_script)
        config_script += "\tend\n"

        # print config_script
        return config_script


class Topology(object):
    def __init__(self, config_path):
        self.switches = {}
        self.generators = {}
        self.config_path = config_path+"generatedconfig/"
        self.topology_file = self.config_path+"Topology.csv"

        self.read_links()
        self.read_rcv_config()
        self.read_pcktgen_config()

    def read_links(self):
        print "reading topology file: ", self.topology_file
        with open(self.topology_file) as topo:
            lines = [e.strip() for e in topo.readlines()]
            for link in lines[lines.index("links:")+1:]:
                if link == "generators:" or link == "receivers:": break
                link_elems = map(int, link.split(","))
                first_sw_id = link_elems[1] / BOX_MULTIPLIER
                second_sw_id = link_elems[2] / BOX_MULTIPLIER
                s1 = self.switches[first_sw_id] if first_sw_id in self.switches else Switch(first_sw_id)
                s2 = self.switches[second_sw_id] if second_sw_id in self.switches else Switch(second_sw_id)
                s1.add_interface(int(link_elems[1]), int(link_elems[2]))
                self.switches[first_sw_id] = s1
                s2.add_interface(int(link_elems[1]), int(link_elems[2]))
                self.switches[second_sw_id] = s2

    def read_pcktgen_config(self):
        """
        Reads the generators: part of the topology file

        """
        print "reading packet  generator configuration file"
        with open(self.topology_file) as packtgen:
            lines =[p.strip() for p in packtgen.readlines()]
            if "generators:" in lines:
                for link in lines[lines.index("generators:")+1:]:
                    if link == "links:" or link == "receivers:": break
                    link_elems = map(int, link.split(","))
                    sw_id = link_elems[1] / BOX_MULTIPLIER
                    gen_id = link_elems[2] / BOX_MULTIPLIER
                    ingress_switch = self.switches[sw_id] if sw_id in self.switches.keys() else Switch(sw_id)
                    ingress_switch.add_gen_interface(link_elems[1], "gen%s-%s" % (gen_id, link_elems[2]))
                    self.switches[sw_id] = ingress_switch
                    generator = self.generators[gen_id] if gen_id in self.generators.keys() else Generator(gen_id)
                    generator.add_interface(link_elems[1], link_elems[2])
                    self.generators[gen_id] = generator

    def read_rcv_config(self):
        """
        Reads the receivers: part of the topology file.
        Receiver is basically a switch with sFlow enabled on it. The sFlow must be configured via a separate script.
        This function only creates a VM for each receiver, and sets up links between the egress port and a receiver.
        The entry in the topology file must be as following:
        entry_id,egress_switch_port_id,receiver_switch_ingress_portid
        0,23,61
        """
        print "reading receiver configuration file"
        with open(self.topology_file, 'r') as rcv:
            lines = [r.strip() for r in rcv.readlines()]
            if "receivers:" in lines:
                for link in lines[lines.index("receivers:")+1:]:
                    if link == "links:" or link == "generators:" : break
		    print link
                    link_elems = map(int, link.split(","))
                    egresw_id = link_elems[1] / BOX_MULTIPLIER
                    rcvsw_id = link_elems[2] / BOX_MULTIPLIER
                    egress_switch = self.switches[egresw_id] if egresw_id in self.switches else Switch(egresw_id)
                    rcv_switch = self.switches[rcvsw_id] if rcvsw_id in self.switches else Switch(rcvsw_id)
                    egress_switch.add_interface(link_elems[1], link_elems[2])
                    self.switches[egresw_id] = egress_switch
                    rcv_switch.add_interface(link_elems[1], link_elems[2])
                    rcv_switch.is_connected_to_collector = True
                    self.switches[rcvsw_id] = rcv_switch

    def to_string(self):
        topology_string = "Vagrant.configure(\"2\") do |config|\n"
        for sw in self.switches.values():
            topology_string += sw.to_string()
        for gen in self.generators.values():
            topology_string += gen.to_string()
        topology_string += "end\n"
        print topology_string
        return topology_string

    def generate_topology(self, path=""):
        print "Saving Vagrantfile"
        vagrant_path = self.config_path+"vagrant/"
        if not os.path.exists(vagrant_path):
            os.makedirs(vagrant_path)
        f = open(vagrant_path + "Vagrantfile", 'w')
        f.write(self.to_string())
        f.close()


def main(topology_file):
    topo = Topology(topology_file)
    topo.generate_topology()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Virtual Machine Generator")
    parser.add_argument('-p', action='store', dest='path')
    parsed= parser.parse_args()
    if not parsed.path.endswith("/"):
        parsed.path += "/"
    main(parsed.path)
    # if len(sys.argv) < 2:
    #     print >> sys.stderr, "Topology file is missing"
    #     sys.exit(-1)
    # if len(sys.argv) < 3:
    #     print "Warning: no output path is specified, output will be saved to current directory"
    #     main(sys.argv[1], "")
    # else:
    #     main(sys.argv[1], sys.argv[2])
