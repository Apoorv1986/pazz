import org.json.simple.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
@SuppressWarnings("deprecation")
public class ConfigurationConverter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boxConfigurationCovnerter(args[0]);
	}
	public static void topologyConverter(String path){

	}
	public  static void boxConfigurationCovnerter(String path){
		if(!path.endsWith("config") && !path.endsWith("config/")){
			System.err.println("This is not the correct configuration directory");
			return;
		}
		String inputPath = "";
		if(!path.endsWith("/")){
			path += "/";
		}

		inputPath+=(path + "rawconfig/boxes/");
		System.out.println(inputPath);
		String outputPath = path;
		File outputFileDirectory = new File(outputPath+"generatedconfig/boxes");
		if(!outputFileDirectory.exists()){
			outputFileDirectory.mkdir();
		}
		outputPath += "generatedconfig/boxes/";

		File f = new File(inputPath);
		if(!f.exists()){
		    System.out.println("path does not exist");
		    return;
		}
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(f.listFiles()));
		Iterator<File> fileIterator = files.iterator();
		while (fileIterator.hasNext()) {
			File file = (File) fileIterator.next();
			String fileName = file.getName();
			if (file.getName().endsWith(".csv") && !file.isDirectory()) {
				String boxId = fileName.substring(3, fileName.length());
				boxId = boxId.substring(0, boxId.length() - 4);
				try (BufferedReader buffReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
					System.out.println("Reading boxes");
					String line;

					JSONObject box = new JSONObject();
					JSONArray tables = new JSONArray();
					JSONObject currentTable = new JSONObject();
					JSONArray tableRules = new JSONArray();
					int currentTableId = 0;

					while ((line = buffReader.readLine()) != null) {
						if (line.startsWith("table")) {
							currentTableId = Integer.parseInt(line.trim().split(":")[1]);
							if (currentTableId != 0) {
								currentTable.put("rules", tableRules);
								currentTable.put("id", currentTableId);
								tables.add(currentTable);
								tableRules = new JSONArray();
								currentTable = new JSONObject();

							}
						} else if (line.startsWith("rule_id")) {
							JSONObject rule = new JSONObject();
							String[] rulesString = line.split(",");
							for (String ruleString : rulesString) {
								String[] keyValue = ruleString.split("=");
								String key = keyValue[0].trim().toLowerCase();
								switch (key) {
								case "predicate":
									rule.put(key, keyValue[1].trim());
									break;
								case "in_ports":
									String ports[] = keyValue[1].trim().split(";");
									JSONArray in_ports = new JSONArray();
									for (String str : ports) {
										in_ports.add(Integer.parseInt(str));
									}
									rule.put(key, in_ports);
									break;
								default:
									rule.put(key, Integer.parseInt(keyValue[1].trim()));
									break;
								}
							}
							tableRules.add(rule);
						}
					}
					currentTable.put("id", currentTableId);
					currentTable.put("rules", tableRules);
					tables.add(currentTable);
					box.put("tables", tables);
					box.put("id", boxId);
					try (FileWriter fi = new FileWriter(outputPath+ boxId + ".json")) {
						fi.write(box.toString());
						System.out.println("Successfully Copied JSON Object to File...");
						System.out.println(box.toString());
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}

			}
		}

	}
}
