import java.io.File;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import fuzz.FuzzClient;
import searchservice.PathServer;
import searchservice.TimeManager;

/**
 * @author saidjawadsaidi and Apoorv Shukla
 *
 */
public class PazzMain {
	private Thread thread;

	public static void main(String[] args) throws Exception {
		PazzMain values = new PazzMain(args);
		PathServer server = new PathServer();
		TimeManager time = new TimeManager();

	        if(!values.configurationPath.endsWith("/"))
				values.configurationPath += "/";
		server.setConfigurationPath(values.configurationPath);
		server.setHeaderLength(values.headerLength);
		server.setTimeManager(time);

		Thread serverThread = new Thread(server);
		//System.out.println("Path Server Thread Created... Starting...");
		serverThread.start();

		Thread.sleep(15000);
		FuzzClient client = new FuzzClient(values.configurationPath,values.headerLength);
		//System.out.println("Fuzz Client Thread Created... Starting...");
		client.start();
	}

	@Option(name = "-p", required = true, usage = "Configuration Path", metaVar = "STRING")
	public String configurationPath = "";

	@Option(name = "-h", required = true, usage = "Header Length", metaVar = "INT")
	public int headerLength;

	// @Option(name = "-min", required = true, usage = "Minimum Topology
	// size(Number of Boxes)", metaVar = "INT")
	// public int minNumberOfBoxes;

	// @Option(name="-h", required=true, usage="header length" , metaVar="INT")
	// public int headerLength;

	// @Option(name = "-v", usage = "print number of paths to screen")
	// public boolean verbose = false;

	// @Option(name = "-o", required = true, usage = "Output File", metaVar =
	// "STRING")
	// public String outputFile = "";

	public PazzMain(String... args) {
		CmdLineParser parser = new CmdLineParser(this);
		try {
			parser.parseArgument(args);

		} catch (CmdLineException e) {

			parser.printUsage(System.err);
			System.exit(-1);
		}
	}

}
