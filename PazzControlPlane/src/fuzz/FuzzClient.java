package fuzz;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.thrift.TException;
import jdd.bdd.BDD;
import networkmodel.*;
import org.apache.commons.net.util.*;

public class FuzzClient implements Runnable {
	private String configurationPath;
	private int headerLength;
	private Network network = null;
	private ReachabilityTree reachabilityTree;
	private BDD bdd;
	private Map<Integer, Rule> networkRules;
	private Thread thread;
	private final String bits = "01";
	private final int bitsLength = bits.length();

	public FuzzClient(String configurationPath, int headerLength) {
		this.configurationPath = configurationPath;
		this.headerLength = headerLength;
		try {
			network = new Network(this.headerLength, this.configurationPath);
			reachabilityTree = new ReachabilityTree(network.getNetworkGraph(), network.getInverseNetworkGraph(),
					network.getNetworkRules(), network.getInRuleList(), network.getBoxes(), network.getHeaderLength());
			this.bdd = reachabilityTree.getBDD();
			this.networkRules = reachabilityTree.getNetworkRules();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void run() {
		//while (true) {
			//Get the inPorts List from the file "InportList.txt"
			ArrayList<Integer> inPortList = network.getInPortList();
			//Get the outPorts List from the file "OutportList.txt"	
			ArrayList<Integer> outPortList = network.getOutPortList();

			for (int inPort : inPortList) {
				for (int outPort : outPortList) {
					int reachableHS = this.reachabilityTree.getHeaderSpace(inPort, outPort);
					//System.out.print("Reachable HS: ");
					//bdd.printSet(reachableHS);
					try {
						// test the hidden Rules for every inPort and outPort in the Lists
						testHiddenRules(inPort, outPort);
					} catch (IOException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					testBitFlips(reachableHS);
				}
			}
		//}
	}

	public void start () {
		//System.out.println("+++++++ Start() Fuzz Client: ++++++++++");
			if (thread == null) {
				thread = new Thread (this);
		        	thread.start ();
			}
	}

	private void testHiddenRules(int inPort, int outPort) throws IOException, InterruptedException{
		//from within all the boxes available on the network declare the "inBox" and "outBox" boxes 
		long startTime = System.nanoTime(); //starting the timer
		Box inBox = network.getBoxes().get(inPort/10);//0
		Box outBox = network.getBoxes().get(outPort/10);//15
		//Get all inBox rules that have inPort as in_port 
		ArrayList<Integer> inRuleList = inBox.findInportToRuleMapping(inPort);
		//Get all outBox rules thata have outPort as out_port
		ArrayList<Integer> outRuleList = outBox.findRuleToPortMapping(outPort);

		int inPortPredicate = bdd.getZero();//initiate inPortPredicate ass BDD "0" 
		//loop through all inRuleList rules
		for(int ruleId: inRuleList){
			Rule inRule = networkRules.get(ruleId);
			//Union of all inRules predicates to get the header space coverage of inBox  
			inPortPredicate = bdd.or(inPortPredicate, inRule.getPredicates()[inPort%10]);
		}

		int outPortPredicate = bdd.getZero();//initiate outPortPredicate ass BDD "0" 
		for(int ruleId: outRuleList){//loop through all outRuleList rules
			Rule outRule = networkRules.get(ruleId);
			int outRule_Predicate = 0;
			//since the predicates are related to in_port not out_port
			//we have to go through rule.predicates[7] to get the predicate related to that rule
			for(int j =1; j<7;j++) {
				if (outRule.getPredicates()[j] != 0) {
					outRule_Predicate = outRule.getPredicates()[j];
				}
			}
			////Union of all outRules predicates to get the header space coverage of outBox 
			outPortPredicate = bdd.or(outPortPredicate, outRule_Predicate);//outRule.getPredicates()[outPort%10]
		}
		//Calculate the difference between outBox HS and inBox HS
		int diffHS = diff(outPortPredicate,inPortPredicate);
		//System.out.println("Diff HS: " + diffHS);
		long stopTime = System.nanoTime();//stopping the timer
		long elapsedTime = stopTime - startTime;
		double seconds = (double)elapsedTime / 1000000000.0;
		System.out.println("Fuzz Calculation time: " + seconds);
		//System.out.println("Final inPortPredicate: "+ convertBDDToString(bdd,inPortPredicate));
		//System.out.println("Final outPortPredicate: "+ convertBDDToString(bdd,outPortPredicate));
		//System.out.println("The diffHeader Space: "+ convertBDDToString(bdd,diffHS));
		//Based on the difference, if != 0 generate fuzz traffic in the diff HS "sweepTest"
		if(diffHS != 0 ){
			sweepTest(diffHS);
			diffHS = outPortPredicate;
		}
		else{
			diffHS = diff(inPortPredicate, outPortPredicate);
			//System.out.println("Diff HS if previous is Sinput>Soutput: " + convertBDDToString(bdd,diffHS));
                        if(diffHS != 0){
				sweepTest(diffHS);
			}
			diffHS = inPortPredicate;
		}
		//After testing the difference we randomly generate fuzz traffic in the uncovered header space area
		try {
			int uncovered = bdd.ref(diff(bdd.getOne(),diffHS));
			if (uncovered != 0)
				randomTest(uncovered);
		} catch (TException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void testBitFlips(int HS){

	}
	private int diff(int a, int b) {
		//int diff = bdd.and(b, (bdd.not(bdd.and(b, a)))); //ORIGINAL
		int diff = bdd.ref(bdd.and(a, (bdd.not(bdd.and(a, b)))));
		return diff;
	}

	private void randomTest(int hs) throws TException, IOException, InterruptedException{
		String stringHS = convertBDDToString(bdd, hs);
		for (String line: stringHS.split("\\n")){
        	        //System.out.println("Random Test NBTB received Header: " + line);
			for (int i=0; i<8; i++) {
				char[] newStringHS = line.toCharArray();
				Random rand = new Random();
				newStringHS[i] = bits.charAt(rand.nextInt(bitsLength));
				String wildcard = new String(newStringHS);
				//-------------send(wildcard);
				for(String ip : toIP(wildcard)) {
					//System.out.println("Random Test NBTB sent IP: " + ip );
					send(ip);
				}
			}
		}
		for (String line: stringHS.split("\\n")) {
			char[] newStringHS = line.toCharArray();
			if (newStringHS[26] == '1') {
				newStringHS[26] = '0';
			}
			else {
				newStringHS[26] = '1';
			}
			String wildcard = new String(newStringHS);
			//System.out.println("Random Test NBSTB received header: " + line);
			for(String ip : toIP(wildcard)) {
				//System.out.println("Random Test NBSTB sent IP: " + ip );
				send(ip);
			}

			//----------------send(wildcard);
		}
	}

	private void sweepTest(int hs) throws IOException, InterruptedException{
		String stringHS = convertBDDToString(bdd, hs);
		for (String line: stringHS.split("\\n")) {
			//System.out.println("Sweep Test received Header: " + line);
			for(String ip : toIP(line)) {
				//System.out.println("Sweep Test result IP: " + ip );
				send(ip);
			}
		}
	}

	private String convertBDDToString(BDD bdd, int hs) {
	    // Create a stream to hold the output
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    PrintStream ps = new PrintStream(baos);
	    // IMPORTANT: Save the old System.out!
	    PrintStream old = System.out;
	    // Tell Java to use the special stream
	    System.setOut(ps);
	    // Print the BDD: goes to the special stream
	    bdd.printSet(hs);
	    // Put things back
	    System.out.flush();
	    System.setOut(old);
	    String stringBDD = baos.toString();//All Lines are included within the string

	    return stringBDD;
	}

	private void send(String header) throws IOException, InterruptedException {
        	Socket s = null;
       		int threshold = 3;
        	int retryCounter = 0;

        	while (retryCounter < threshold) {
        	  	try {
        	   		retryCounter++;
        	   		s = new Socket("192.168.20.20", 9955);//192.168.20.20 localhost
		   		break;
        	  	} catch (Exception e) {
        	    		System.out.println("Fuzz Server unreachable!");
        	    		Thread.sleep(10000);
        	  	}
        	}
        	if (s == null) {
        		System.out.println("Fuzz Server not connected :( ");
        	}
		else {
        		try {
        			PrintWriter pw = new PrintWriter(s.getOutputStream(), false);
        	       		pw.print(header);
				pw.flush();
        		}
        		finally {
		    		s.close();
			}
		}
	   }

	private String[] toIP(String header) {

		String ip_address ="";
		int mask = 0;
		int start = 0, end = 8;
		int[] addr = new int[4];

		for (int i = 0; i < 4; i++) {
			String addr_substring = header.substring(start,end);
			if(start != 24) {
				addr_substring = addr_substring.replaceAll("-", "0");
				addr[i] = Integer.parseInt(addr_substring, 2);
				if(i == 0) {
					if (addr[i] < 127) {mask = 8;}
					else if (addr[i] < 191 && addr[i] >= 128) {mask = 16;}
					else mask = 24;
				}
			}
			else {
				mask = 32 - (addr_substring.length() - addr_substring.replace("-", "").length());
				addr_substring = addr_substring.replaceAll("-", "0");
				addr[i] = Integer.parseInt(addr_substring, 2);
			}
			start += 8;
			end += 8;
		}
		ip_address = addr[0] + "." + addr[1] + "." + addr[2] + "." + addr[3];

		SubnetUtils utils = new SubnetUtils(ip_address + "/" + mask);
		String[] allIps = utils.getInfo().getAllAddresses();

	return allIps;
	}

}
