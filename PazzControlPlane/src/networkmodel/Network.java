package networkmodel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import jdd.bdd.BDD;

/**
 * @author ApoorvShukla and saidjawadsaidi
 *
 */
@SuppressWarnings("deprecation")
public class Network {

	public BDD bdd;
	private Map<Integer, Box> boxes = new LinkedHashMap<Integer, Box>();
	private Map<Integer, ArrayList<Integer>> inverseNetworkGraph = new LinkedHashMap<Integer, ArrayList<Integer>>();
	// Graph of a ruleId to a list of connected ruleIds
	private Map<Integer, ArrayList<Integer>> networkGraph = new HashMap<Integer, ArrayList<Integer>>();
	private Map<Integer, Rule> networkRules = new LinkedHashMap<Integer, Rule>();
	private Map<Integer, Integer> topology = new LinkedHashMap<Integer, Integer>();
	private ArrayList<Integer> inPortList = new ArrayList<Integer>();
	private ArrayList<Integer> outPortList = new ArrayList<Integer>();

	private String configurationPath ;
	private int headerLength;

	public Network(int headerLength, String configurationPath) throws Exception {
		this.bdd = new BDD(10000, 10000);
		if(new File(configurationPath).exists()){
			this.configurationPath = configurationPath;
			for (int i = 0; i < headerLength; i++) {
				bdd.createVar();
			}
			this.headerLength = headerLength;
			loadConfiguration();
			generateNetworkGraph();
		}else{
			throw new Exception("Invalid Configuration Path. Please make sure you've generated the required configuration for runing controlplane component");
		}
	}

	public void cleanUp() {
		bdd.cleanup();
	}

	/**
	 * @return the headerLength
	 */
	public int getHeaderLength() {
		return headerLength;
	}


	private void generateNetworkGraph() {
		Iterator<Integer> boxIterator = boxes.keySet().iterator();
		// Iterate in all boxes in the network
		while (boxIterator.hasNext()) {
			Box box = boxes.get(boxIterator.next());
			Map<Integer, ArrayList<Integer>> tRules = box.getRules();
			int tableId;
			Iterator<Integer> tableIterator = tRules.keySet().iterator();
			// Inside each box iterate every table
			while (tableIterator.hasNext()) {
				tableId = tableIterator.next();
				ArrayList<Integer> tableRules = tRules.get(tableId);
				// For each rule inside a table:
				for (int ruleId : tableRules) {
					ArrayList<Integer> nextRules = new ArrayList<Integer>();
					Rule rule = networkRules.get(ruleId);
					// If the rule drops a packet, the output port should be set to -1
					if (rule.out_ports == -1)
						continue;
					// Find the next possible boxes by checking the topology for a physical link
					if (topology.containsKey(rule.out_ports)) {
						// Retrieve the next physical port
						int nextPhysicalPort = topology.get(rule.out_ports);
						// Find the next Box Identifier from the nextPort identifier
						int nextBoxId = nextPhysicalPort / 10;
						Box nextBox = boxes.get(nextBoxId);
						// Check the rules for the first table only "table 0"
						ArrayList<Integer> nextBoxRules = nextBox.getRules().get(0);
						// Find the rule which its inport is connected to
						// current rule, and its predicate overlaps with current rule
						for (int nextBoxRuleId : nextBoxRules) {
							Rule nextRule = networkRules.get(nextBoxRuleId);
							int portIndex = nextPhysicalPort % 10;
							int nextRulePredicate = nextRule.getPredicates()[portIndex];
							for (int localIndex = 1; localIndex < 7; localIndex++) {
								int rulePredicate = rule.getPredicates()[localIndex];
								if (rulePredicate != 0 && nextRulePredicate != 0 && bdd.and(rulePredicate, nextRulePredicate) != 0) {
									nextRules.add(nextBoxRuleId);
									break;
								}
							}
						}
					}

					if (!nextRules.isEmpty()) {
						networkGraph.put(ruleId, nextRules);
						for (int nextRuleIds : nextRules) {
							ArrayList<Integer> connectedRuleIds = (inverseNetworkGraph.containsKey(nextRuleIds)?inverseNetworkGraph.get(nextRuleIds):new ArrayList<Integer>());
								if (connectedRuleIds.indexOf(ruleId) == -1) {
									connectedRuleIds.add(ruleId);
									inverseNetworkGraph.put(nextRuleIds, connectedRuleIds);
							}
						}
					}
				}
			}

		}
		//cleanUp();
		System.out.println("The network graph is " + networkGraph.toString());
		System.out.println("The network graph is " + inverseNetworkGraph.toString());

	}

	/**
	 * @return the boxes
	 */
	public Map<Integer, Box> getBoxes() {
		return boxes;
	}

	/**
	 * @return the inverseNetworkGraph
	 */
	public Map<Integer, ArrayList<Integer>> getInverseNetworkGraph() {
		return inverseNetworkGraph;
	}

//	public ReachabilityTreeGenerator getGenerator() {
//		ReachabilityTreeGenerator generator = null;
//		generator = new ReachabilityTreeGenerator(networkGraph,inverseNetworkGraph,networkRules,bdd);
//
//		return generator;
//
//	}

	/**
	 * @return the networkGraph
	 */
	public Map<Integer, ArrayList<Integer>> getNetworkGraph() {
		return networkGraph;
	}

	/**
	 * @return the networkRules
	 */
	public Map<Integer, Rule> getNetworkRules() {
		return networkRules;
	}

	public ArrayList<Integer> getOutPortList(){
		if(!this.outPortList.isEmpty())
			return outPortList;
		File file = new File(configurationPath+"generatedconfig/OutportList.txt");
		try (BufferedReader buffReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
			String line = buffReader.readLine();
			String[] portList = line.split(",");
			for(String portId: portList){
				int port = Integer.parseInt(portId);
				this.outPortList.add(port);
				}
			System.out.println("The outPort List: " + Arrays.toString(outPortList.toArray()));
			return outPortList;

			}catch(IOException ex){
				return null;
			}
	}

	public ArrayList<Integer> getInPortList(){
		if(!this.inPortList.isEmpty())
			return this.inPortList;
		File file = new File(configurationPath+"generatedconfig/InportList.txt");
		try (BufferedReader buffReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
			String line = buffReader.readLine();
			String[] portList = line.split(",");
			for(String portId: portList){
				int port = Integer.parseInt(portId);
				this.inPortList.add(port);
				}

			System.out.println("The inPort List: " + Arrays.toString(inPortList.toArray()));
			return inPortList;

			}catch(IOException ex){
				return null;
			}
	}


	public ArrayList<Integer> getInRuleList(){

		File file = new File(configurationPath+"generatedconfig/InportList.txt");
		try (BufferedReader buffReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
			String line = buffReader.readLine();
			String[] portList = line.split(",");
			ArrayList<Integer> inRuleList = new ArrayList<Integer>();
			for(String portId: portList){
				int port = Integer.parseInt(portId);
				Box box = boxes.get(port/10);
					inRuleList.addAll(box.findInportToRuleMapping(port));
				}

			System.out.println("The inRuleList loaded: " + Arrays.toString(inRuleList.toArray()));
			return inRuleList;

			}catch(IOException ex){
				return null;
			}
	}

	public void loadBoxes() {
		File f = new File(configurationPath+"generatedconfig/boxes/");
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(f.listFiles()));
		Iterator<File> fileIterator = files.iterator();
		JSONParser parser = new JSONParser();
		while (fileIterator.hasNext()) {
			File file = fileIterator.next();
			String fileName = file.getName();
			if (fileName.endsWith(".json") && file.isFile()) {
				JSONObject box;
				try {
					System.out.println("File name is" + fileName);
					box = (JSONObject) parser.parse(new FileReader(file.getAbsolutePath()));
					int boxId = Integer.parseInt((String) box.get("id"));
					Box newBox = new Box(bdd, boxId, networkRules);
					JSONArray tables = (JSONArray) box.get("tables");
					for (int k = 0; k < tables.size(); k++) {
						JSONObject table = (JSONObject) tables.get(k);
						int table_id = (int) ((long) (table.get("id")));
						JSONArray ruleStrings = (JSONArray) table.get("rules");
						ArrayList<Rule> rules = new ArrayList<Rule>();
						rules.clear();
						for (int i = 0; i < ruleStrings.size(); i++) {
							JSONObject ruleObject = (JSONObject) ruleStrings.get(i);
							int rule_id = (int) ((long) ruleObject.get("rule_id"));
							int priority = (int) ((long) ruleObject.get("priority"));
							JSONArray in_port_strings = (JSONArray) ruleObject.get("in_ports");
							int[] in_ports = new int[in_port_strings.size()];
							for (int j = 0; j < in_ports.length; j++) {
								in_ports[j] = (int) ((long) in_port_strings.get(j));
							}
							int out_port = (int) ((long) ruleObject.get("out_ports"));
							String rulePredicateString = (String) ruleObject.get("predicate");
							int rulePredicate = bdd.ref(bdd.minterm(rulePredicateString));

							Rule rule = new Rule(rule_id, in_ports, out_port, priority, table_id, rulePredicateString);

							for (int portIndex : in_ports) {
								rule.setPredicateForPort(portIndex % 10, rulePredicate);
							}
							networkRules.put(rule_id, rule);
							rules.add(rule);
						}
						newBox.addRules(rules.toArray(new Rule[rules.size()]), table_id);
						boxes.put(boxId, newBox);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadConfiguration() {
			loadBoxes();
			loadTopology();
	}

	private void loadTopology() {
		File file = new File(configurationPath+"generatedconfig/Topology.csv");
		try (BufferedReader buffReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
			String line = buffReader.readLine();
			while (!(line = buffReader.readLine()).equals("generators:")) {
				String[] values = line.split(",");
				int in_port = Integer.parseInt(values[1]);
				int out_port = Integer.parseInt(values[2]);
				topology.put(in_port, out_port);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
