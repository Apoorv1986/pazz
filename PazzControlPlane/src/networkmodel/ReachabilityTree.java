package networkmodel;

import java.util.ArrayList;
import java.util.Map;
import java.util.Stack;

import jdd.bdd.BDD;

public class ReachabilityTree {

	BDD bdd;
	Map<Integer, Box> boxes;
	Stack<Integer> currentPathInt = new Stack<Integer>();
	Stack<Integer> currentPredicates = new Stack<Integer>();
	Stack<Integer> currentPorts = new Stack<Integer>();

	Map<Integer, ArrayList<Integer>> inverseNetworkGraph;
	Map<Integer, ArrayList<Integer>> networkGraph;
	Map<Integer, Rule> networkRules;
	ArrayList<Integer> inRuleList = new ArrayList<Integer>();

	ArrayList<Integer> onList = new ArrayList<Integer>();
	ArrayList<ArrayList<Integer>> pathPredicates = new ArrayList<ArrayList<Integer>>();
	ArrayList<ArrayList<Integer>> paths = new ArrayList<ArrayList<Integer>>();
	ArrayList<ArrayList<Integer>> ports = new ArrayList<ArrayList<Integer>>();
	ArrayList<Integer> predicates = new ArrayList<Integer>();
	Rule outRule;

	PathWrapper wrapper = null;
	int headerLength = 0;

	public ReachabilityTree(Map<Integer, ArrayList<Integer>> networkGraph,
			Map<Integer, ArrayList<Integer>> inverseNetworkGraph, Map<Integer, Rule> networkRules,
			ArrayList<Integer> inRuleList, Map<Integer, Box> boxes, int headerLength) {
		this.networkGraph = networkGraph;
		System.out.println("network graph s"+networkGraph.toString());
		this.inverseNetworkGraph = inverseNetworkGraph;
		System.out.println("Inverse network graph is" + inverseNetworkGraph.toString());

		this.networkRules = networkRules;
		this.inRuleList = inRuleList;
		this.boxes = boxes;
		this.headerLength = headerLength;
		bdd = new BDD(10000, 10000);
		for (int i = 0; i < headerLength; i++) {
			bdd.ref(bdd.createVar());
		}
		initRules();
	}
	public BDD getBDD(){
		return this.bdd;
	}

	public PathWrapper findReversePath(int egressPort, String packetHeader) {
		System.out.println("Request is in reachability tree" + egressPort + " packet header " + packetHeader
				+ "packet header length is " + packetHeader.length());
		int predicate = bdd.ref(bdd.minterm(packetHeader));
		System.out.println("the predicate is" + predicate);
		generateInverseReachabilityFromPort(egressPort, predicate);
		return getPathsAndPorts();

	}

	public int getHeaderSpace(int inPortId, int outPortId){
		Box inBox = boxes.get(inPortId/10);
		Box outBox = boxes.get(outPortId/10);
		reInitialize();
		for(int inRuleId: inBox.findInportToRuleMapping(inPortId)){
			for(int outRuleId: outBox.findRuleToPortMapping(outPortId)){
				this.outRule = networkRules.get(outRuleId);
				generateReachability(networkRules.get(inRuleId), bdd.getOne());
			}
		}
		int union = bdd.getZero();
		for(int pred: predicates){
			union = bdd.orTo(pred, union);
		}
		return bdd.ref(union);
	}

	private void generateReachability(Rule inRule, int predicate) {
		int newPredicate = 0;
		int[] inRulePredicates = inRule.getPredicates();
		for (int inPort = 1; inPort < 7; inPort++) {
			int pred = inRulePredicates[inPort];
			 System.out.println("Inverse Reachability, rule predicate is "+ pred
			 + " rule is "+ inRule.rule_id);
			if (pred != 0) {
				newPredicate = bdd.andTo(pred, predicate);
				 System.out.println("new predicate is "+ newPredicate);
				if (newPredicate != 0) {
					currentPathInt.push(inRule.rule_id);
					currentPorts.push(inPort);
					currentPredicates.push(newPredicate);
					if (onList.indexOf(inRule.rule_id) == -1)
						onList.add(inRule.rule_id);
					// Check the set of possible input rules
					if (inRule.rule_id == outRule.rule_id) {
						bdd.ref(newPredicate);
						predicates.add(newPredicate);
					} else {
						ArrayList<Integer> rules = null;
						rules = networkGraph.get(inRule.rule_id);
						if (rules != null)
							for (int rule : rules) {
								if (onList.indexOf(rule) == -1) {
									generateReachability(networkRules.get(rule), newPredicate);
								}
							}
					}
				}
			}
		}
		if (!currentPathInt.isEmpty())
			currentPathInt.pop();
		if (onList.indexOf(inRule.rule_id) != -1)
			onList.remove(onList.indexOf(inRule.rule_id));
		if (!currentPorts.isEmpty())
			currentPorts.pop();
		// if (!currentPredicates.isEmpty())
		// currentPredicates.pop();

	}

	private void generateInverseReachability(Rule inRule, int predicate) {
		int newPredicate = 0;
		int[] inRulePredicates = inRule.getPredicates();
		for (int inPort = 1; inPort < 7; inPort++) {
			int pred = inRulePredicates[inPort];
			 System.out.println("Inverse Reachability, rule predicate is"+pred
			 + "rule is is "+ inRule.rule_id);
			if (pred != 0) {
				newPredicate = bdd.andTo(pred, predicate);
				System.out.println("new predicate is" + newPredicate);
				if (newPredicate != 0) {
					currentPathInt.push(inRule.rule_id);
					currentPorts.push(inPort);
					// currentPredicates.push(newPredicate);
					if (onList.indexOf(inRule.rule_id) == -1)
						onList.add(inRule.rule_id);
					// Check the set of possible input rules
					if (inRuleList.indexOf(inRule.rule_id) != -1) {
						paths.add(new ArrayList<Integer>(currentPathInt));
						ports.add(new ArrayList<Integer>(currentPorts));
						// pathPredicates.add(new
						// ArrayList<Integer>(currentPredicates));
						// for (int pr : currentPredicates) {
						// bdd.ref(pr);
						// }
					} else {
						// System.out.println("Getting previous rules");
						// System.out.println("Previous rules are"+
						// inverseNetworkGraph.toString());

						ArrayList<Integer> rules = null;
						rules = inverseNetworkGraph.get(inRule.rule_id);
						if (rules != null)
							for (int rule : rules) {
								if (onList.indexOf(rule) == -1) {
									generateInverseReachability(networkRules.get(rule), newPredicate);
								}
							}
					}
				}
			}
		}
		if (!currentPathInt.isEmpty())
			currentPathInt.pop();
		if (onList.indexOf(inRule.rule_id) != -1)
			onList.remove(onList.indexOf(inRule.rule_id));
		if (!currentPorts.isEmpty())
			currentPorts.pop();
		// if (!currentPredicates.isEmpty())
		// currentPredicates.pop();

	}

	public void setOutRule(Rule rule) {
		this.outRule = rule;
	}

	private void generateInverseReachabilityFromPort(int portId, int predicate) {
		Box box = boxes.get(portId / 10);
		wrapper = new PathWrapper();
		for (int ruleId : box.findRuleToPortMapping(portId)) {
			System.out.println("RuleID is " + ruleId);
			generateInverseReachability(networkRules.get(ruleId), predicate);
			if (!paths.isEmpty()) {
				//System.out.println("adding paths");
				wrapper.addPaths(paths);
				wrapper.addPorts(ports);
				// wrapper.addPredicates(pathPredicates);

			}
			reInitialize();
			//System.out.println("reinitializing " + wrapper.pathStrings().toString());

		}
		System.out.println("Path found " + wrapper.pathStrings().toString());

	}

	private PathWrapper getPathsAndPorts() {
		//System.out.println("Get path and predicate");

		PathWrapper deepCopiedWrapper = new PathWrapper(wrapper);
		//System.out.println("Get Paths and predicates: " + wrapper.pathStrings().toString());
		//System.out.println("portList is" + wrapper.portStrings().toString());
		wrapper = null;
		return deepCopiedWrapper;
	}

	private void initRules() {
		for (Box box : boxes.values()) {
			for (int tableId : box.tRules.keySet()) {
				ArrayList<Integer> rules = box.tRules.get(tableId);
				for (int ruleId : rules) {
					networkRules.get(ruleId).reCalculatePredicate(bdd);
				}
				box.setBddObj(bdd);
				box.setNetworkRules(networkRules);
				box.recalculatePredicates(tableId);
			}
		}
	}
	
	/**
	 * @return the networkGraph
	 */
	public Map<Integer, ArrayList<Integer>> getNetworkGraph() {
		return networkGraph;
	}
	/**
	 * @param networkGraph the networkGraph to set
	 */
	public void setNetworkGraph(Map<Integer, ArrayList<Integer>> networkGraph) {
		this.networkGraph = networkGraph;
	}
	/**
	 * @return the networkRules
	 */
	public Map<Integer, Rule> getNetworkRules() {
		return networkRules;
	}
	/**
	 * @param networkRules the networkRules to set
	 */
	public void setNetworkRules(Map<Integer, Rule> networkRules) {
		this.networkRules = networkRules;
	}
	private void reInitialize() {
		currentPathInt.clear();
		currentPorts.clear();
		predicates.clear();
		this.outRule = null;
		// currentPredicates.clear();
		paths.clear();
		ports.clear();
		onList.clear();
		// pathPredicates.clear();

	}

}
