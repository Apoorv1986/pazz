package networkmodel;

import jdd.bdd.*;
import jdd.util.*;
import jdd.util.Dot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
public class Experiment {

	public static void main(String[] args) {
		BDD bdd = new BDD(1000,1000);
		
		int v1 = bdd.ref(bdd.createVar());
		int v2 = bdd.ref(bdd.createVar());
		int v3 = bdd.ref(bdd.createVar());
		int v4 = bdd.ref(bdd.createVar());
		int bdd1 = bdd.ref( bdd.and(v1, bdd.not(v2)) );
		int bdd2 = bdd.ref( bdd.and(bdd.not(v3), bdd.not(v4) ));
		int bdd3 = bdd.ref( bdd.or( bdd1, bdd2) ); 
		int bdd4 = bdd.ref(bdd.and(bdd2, v1));
		//System.out.println(v1 + " " + v2);
		//System.out.println(bdd1 + " " + bdd2+ " " + bdd3);
		int andResult = bdd3;
		Dot.setRemoveDotFile(false);
		Dot.setExecuteDot(false);
		//int v1 = bdd.ref(bdd.createVar());
		//int v2 = bdd.ref(bdd.createVar());
		//int x = bdd.ref(bdd.createVar());
		//int cube = bdd.ref( bdd.and(v1,v2) );
		//int x2 = bdd.ref( bdd.forall(x,cube) );
		//bdd.printCubes(x2);
		//bdd.printSet(x2);
		//bdd.printDot("Result", x2);
		for (int i = 0; i < 6 ; i++) {
			bdd.createVar();
		}
		boolean bits[] = new boolean[6];
		bits[0] = false;
		bits[1] = false;
		bits[2] = true;
		bits[3] = false;
		bits[4] = false;
		bits[5] = true;
		int bdd5 = bdd.ref(bdd.minterm("00100-"));
		int bdd6 = bdd.ref(bdd.minterm("1010--"));
		int bdd7 = bdd.ref(bdd.and(bdd6,(bdd.not(bdd.and(bdd5,bdd6)))));
		int bdd8 = bdd.ref(bdd.or(bdd6, bdd5));
		//System.out.println("Difference" + bdd7);
		//System.out.println("DifferenceX" + bdd8);
		bdd.print(bdd7);
		bdd.printCubes(bdd7);
		bdd.printSet(bdd7);
		Dot.setRemoveDotFile(false);
		Dot.setExecuteDot(false);
		bdd.printDot("Difference", bdd7);
		bdd.printDot("DifferenceX", bdd8);
		/*bdd.printDot("v1",v1);
		bdd.printDot("v2",v2);
		bdd.printDot("v3",v3);
		bdd.printDot("v4",v4);
		bdd.printDot("bdd1",bdd1);
		bdd.printDot("bdd2",bdd2);
		
		System.out.println("print dot: " );

		bdd.printDot("Result", andResult);
		
		int v5 = bdd.ref(bdd.and(v1, bdd.not(v2)));
		bdd.printCubes(v1);
		bdd.printCubes(v2);
		bdd.printCubes(andResult
				);
		bdd.printSet(v5);
		bdd.printCubes(v5);
		System.out.println("PrinCubt: " );
		*/
			}
}
