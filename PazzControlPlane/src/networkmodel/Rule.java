package networkmodel;

import jdd.bdd.BDD;

/**
 * @author ApoorvShukla and saidjawadsaidi
 *
 */
/**
 * @author saidjawadsaidi
 *
 */
public class Rule {
	int[] in_ports;
	String matchString;
	int out_ports;
	int predicate = 0;
	int[] predicates = new int[7];
	int priority;
	int rule_id;
	int table_id;


	/**
	 * @param id
	 * @param in_ports
	 * @param out_ports
	 * @param priority
	 * @param table_id
	 * @param matchString
	 */
	public Rule(int id, int[] in_ports, int out_ports, int priority, int table_id, String matchString) {
		this.rule_id = id;
		this.in_ports = in_ports;
		this.out_ports = out_ports;
		this.priority = priority;
		this.table_id = table_id;
		this.matchString = matchString;

	}

	/**
	 * @return
	 */
	public int[] getin_ports() {
		return this.in_ports;
	}

	/**
	 * @return the matchString
	 */
	public String getMatchString() {
		return matchString;
	}

	/**
	 * @return
	 */
	public int getOutportId() {
		return this.out_ports;
	}

	public int getPredicate() {
		return this.predicate;
	}

	public int[] getPredicates() {
		return predicates;
	}

	public int getPriority() {
		return priority;
	}

	public int getrule_id() {
		return this.rule_id;
	}

	public int gettable_id() {
		return table_id;
	}

	public void reSetAllPredicates(int predicate) {
		this.predicate = predicate;
		for (int port : in_ports) {
			predicates[port % 10] = predicate;
		}

	}
	
	public boolean matchesInportId(int portId){
		for(int pId: in_ports){
			if(pId==portId)
				return true;
		}
		return false;
	}

	/**
	 * @param matchString
	 *            the matchString to set
	 */
	public void setMatchString(String matchString) {
		this.matchString = matchString;
	}

	public void setPredicateForPort(int portIndex, int predicate) {
		predicates[portIndex] = predicate;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void settable_id(int table_id) {
		this.table_id = table_id;
	}
	public void reCalculatePredicate(BDD bdd){
		int predicate = bdd.ref(bdd.minterm(this.matchString));
		this.predicates = new int[7];
		for (int port : in_ports) {
			predicates[port % 10] = predicate;
		}
	}
}
