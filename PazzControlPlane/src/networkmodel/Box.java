package networkmodel;

import jdd.bdd.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Jawad Saidi and Apoorv Shukla
 *
 */

public class Box {
	BDD bddObj;
	int boxId;
	Map<Integer, Rule> networkRules;
	Map<Integer, ArrayList<Integer>> tRules = new LinkedHashMap<Integer, ArrayList<Integer>>();

	public Box(BDD bdd, int boxId, Map<Integer, Rule> networkRules) {
		this.bddObj = bdd;
		this.boxId = boxId;
		this.networkRules = networkRules;
	}

	// In order to incorporate the dynamicity in the form of adding a new rule,
	// rule predicates must be recalculated
	public void addRule(Rule rule) {
		int rule_id = rule.getrule_id();
		int predicate = rule.getPredicate();
		int table_id = rule.gettable_id();
		int priority = rule.getPriority();
		ArrayList<Integer> table;
		// if we have the table retrieve it.
		if (tRules.containsKey(table_id)) {
			table = tRules.get(table_id);
			int position = 0;
			boolean found = false;
			// finding the position of newly added rule according to its
			// priority
			for (position = 0; position < table.size(); position++) {
				// retrieve the rule object according to the rule_id and check
				// the priority
				if (networkRules.get((table.get(position))).priority <= priority) {
					table.add(position, rule_id);
					found = true;
					break;

				}
			}
			if (!found)
				table.add(rule_id);

			// System.out.println("Rule added, ID="+rule_id+"
			// priority="+priority);
			// System.out.println("Currrent List"+table.toString());
			// //Subtract the match domain of higher priority rules from lower
			// priority rules
			recalculatePredicates(table_id);
		}
		// otherwise create a new one.
		else {
			table = new ArrayList<Integer>();
			table.add(rule_id);
			tRules.put(table_id, table);
		}
	}

	// Add a bunch of rules to a box i.e. populate the box rules
	public void addRules(Rule rules[], int table_id) {
		int pos = 0;
		ArrayList<Integer> table;
		if (tRules.containsKey(table_id)) {
			table = tRules.get(table_id);
		} else {
			table = new ArrayList<Integer>();
			tRules.put(table_id, table);
		}
		while (pos < rules.length) {
			Rule rule = rules[pos];
			int rule_id = rule.getrule_id();
			int priority = rule.getPriority();
			int position;
			boolean found = false;
			for (position = 0; position < table.size(); position++) {
				// retrieve the rule object according to the rule_id and check
				// the priority
				if (networkRules.get((table.get(position))).priority <= priority) {
					table.add(position, rule_id);
					found = true;
					break;

				}
			}
			if (!found)
				table.add(rule_id);
			pos++;
		}
		recalculatePredicates(table_id);
	}

	private int diff(int a, int b) {
		int diff = bddObj.and(b, (bddObj.not(bddObj.and(b, a))));
		return diff;
	}

	/**
	 * @return the bddObj
	 */
	public BDD getBddObj() {
		return bddObj;
	}
	
	public ArrayList<Integer> findRuleToPortMapping(int portId){
		ArrayList<Integer> rules = new ArrayList<Integer>();
		for(ArrayList<Integer> ruleIds: tRules.values()){
			for(int rule: ruleIds){
				if(networkRules.get(rule).getOutportId() == portId){
					rules.add(rule);
				}
			}
		}
		return rules;
	}
	
	public ArrayList<Integer> findInportToRuleMapping(int portId){
		ArrayList<Integer> rules = new ArrayList<Integer>();
		for(int ruleId: tRules.get(0)){
				if(networkRules.get(ruleId).matchesInportId(portId)){
					rules.add(ruleId);
				}
			}
		return rules;
	}
	
	public Map<Integer, ArrayList<Integer>> getRules() {
		return tRules;
	}

	public Map<Integer, ArrayList<Integer>> gettRules() {
		return tRules;
	}

	// In a table, for each rule, subtract the domain from the domain of lower
	// priority rules
	public void recalculatePredicates(int table_id) {
		ArrayList<Integer> rules = tRules.get(table_id);
		int higherPriority_Rule_Id;
		int lowerPriority_Rule_Id;
		int higherPriority_Rule_Predicate;
		int lowerPriority_Rule_Predicate;
		if (rules.size() < 1)
			return;
		// We assume that box has 6 ports starting from 1-6, for each port
		// subtract the predicate of lower priority rules from the higher
		// priority rules
		for (int inPort_Index = 1; inPort_Index < 7; inPort_Index++) {
			for (int i = 1; i < rules.size(); i++) {
				// compare list.get(i) and list.get(j)
				lowerPriority_Rule_Id = rules.get(i);
				Rule rule = networkRules.get(lowerPriority_Rule_Id);
				lowerPriority_Rule_Predicate = rule.getPredicates()[inPort_Index];
				// If the lower priority predicate is 0, it means that it will
				// not match on that port
				if (lowerPriority_Rule_Predicate != 0) {
					int lowerPriority_Rule_Predicate_OldValue = lowerPriority_Rule_Predicate;
					for (int j = 0; j < i; j++) {
						// Retrieve the rule object according to the rule_id and
						// calculate difference of lower and higher priority
						// rules
						higherPriority_Rule_Id = rules.get(j);
						higherPriority_Rule_Predicate = networkRules.get(higherPriority_Rule_Id)
								.getPredicates()[inPort_Index];
						// if the higher priority predicate is 0, then the rule
						// will not match on that port
						if (higherPriority_Rule_Predicate != 0)
							lowerPriority_Rule_Predicate = diff(higherPriority_Rule_Predicate, lowerPriority_Rule_Predicate);

					}
					// Since we don't need the match predicate of the rule
					// anymore, deref it from BDD to save memory
					bddObj.deref(lowerPriority_Rule_Predicate_OldValue);
					// Update the rule with the new predicate with respect to
					// its priority
					rule.setPredicateForPort(inPort_Index, bddObj.ref(lowerPriority_Rule_Predicate));
					// Dot.setRemoveDotFile(false);
					// Dot.setExecuteDot(false);
					// bddObj.printDot("Difference",
					// lowerPriorityRulePredicate);
					//
					//System.out.println("the predicate for " + lowerPriorityrule_id + " at port_index" + in_portIndex + " is " + lowerPriorityRulePredicate);
				}
			}
		}
	}

	/**
	 * @param networkRules the networkRules to set
	 */
	public void setNetworkRules(Map<Integer, Rule> networkRules) {
		this.networkRules = networkRules;
	}

	/**
	 * @param bddObj
	 *            the bddObj to set
	 */
	public void setBddObj(BDD bddObj) {
		this.bddObj = bddObj;
	}
}
