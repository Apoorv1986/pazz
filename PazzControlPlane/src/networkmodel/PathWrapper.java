package networkmodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import jdd.bdd.BDD; 

public class PathWrapper {
	public PathWrapper(){
		
	}
	public PathWrapper(PathWrapper wrapper){
		bdd = wrapper.bdd;
		rules.addAll(wrapper.getPaths());
		predicates.addAll(wrapper.getPredicates());
		ports.addAll(wrapper.getPorts());

	    networkRules.putAll(wrapper.getNetworkRules());
		//System.out.println("PathWrapper constructor adding paths"+ this.getPaths().toString());

	}
	 /**
	 * @return the networkRules
	 */
	public Map<Integer, Rule> getNetworkRules() {
		return networkRules;
	}
	public BDD bdd = null;
	 ArrayList<ArrayList<Integer>> rules = new ArrayList<ArrayList<Integer>>();
	 ArrayList<ArrayList<Integer>> predicates = new ArrayList<ArrayList<Integer>>();
	 ArrayList<ArrayList<Integer>> ports = new ArrayList<ArrayList<Integer>>();
	 
	 /**
	 * @return the ports
	 */
	public ArrayList<ArrayList<Integer>> getPorts() {
		return ports;
	}
	
	/**
	 * @param ports the ports to set
	 */
	public void addPorts(ArrayList<ArrayList<Integer>> ports) {
		this.ports.addAll(ports);
	}
	
	Map<Integer, Rule> networkRules = new LinkedHashMap<Integer, Rule>();
	 
	 
	/**
	 * @param networkRules the networkRules to set
	 */
	public void setNetworkRules(Map<Integer, Rule> networkRules) {
		this.networkRules = networkRules;
	}
	/**
	 * @return the paths
	 */
	public ArrayList<ArrayList<Integer>> getPaths() {
		return rules;
	}
	/**
	 * @param paths the paths to set
	 */
	public void addPaths(ArrayList<ArrayList<Integer>> paths) {
		this.rules.addAll(paths);
	}
	/**
	 * @return the predicates
	 */
	public ArrayList<ArrayList<Integer>> getPredicates() {
		return predicates;
	}
	/**
	 * @param predicates the predicates to set
	 */
	public void addPredicates(ArrayList<ArrayList<Integer>> predicates) {
		this.predicates.addAll(predicates);
	} 
	
	
	public void printStats(){
		//System.out.println("Total Number of paths were "+rules.size());
	}
	
	public ArrayList<String> pathStrings(){
		ArrayList<String> pathString = new ArrayList<String>();
		for(ArrayList<Integer> path: rules){
			pathString.add(path.toString());
		}
		return pathString;
	}
	
	public ArrayList<String> portStrings(){
		ArrayList<String> portString = new ArrayList<String>();
		for(ArrayList<Integer> port: ports){
			portString.add(port.toString());
		}
		return portString;
	}
	
	public ArrayList<String> pathAndPortStrings(){
		ArrayList<String> portPathString = new ArrayList<String>();
		portPathString.add("rules:" );
		portPathString.addAll(pathStrings());
		portPathString.add("ports:");
		portPathString.addAll(portStrings());
		return portPathString;
	}
	
	public void printPaths(){
		Iterator<ArrayList<Integer>> pathListIterator = rules.iterator();
		Iterator<ArrayList<Integer>> predicateListIterator = predicates.iterator();
		while(pathListIterator.hasNext() && predicateListIterator.hasNext()){
			ArrayList<Integer> rules = pathListIterator.next();
			ArrayList<Integer> predicates = predicateListIterator.next();
			Iterator<Integer> ruleIterator = rules.iterator();
			Iterator<Integer> predicateIterator = predicates.iterator();
			//System.out.println("Path Number: "+rules.indexOf(rules));
				Rule rule = null;
				int predicate = 0 ;
			while(ruleIterator.hasNext() && predicateIterator.hasNext()){
				rule = networkRules.get(ruleIterator.next());
				predicate = predicateIterator.next();
				//System.out.print("Rule ID:"+rule.rule_id+" Predicate: "+predicate+"--->");
			}
			//System.out.println();
			
		}
		
	}
	
}
