package searchservice;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;

import networkmodel.Network;
import networkmodel.ReachabilityTree;

import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;

// Generated code

import java.util.HashMap;

public class PathServer implements Runnable {

  public  PathServiceImpl handler;
  @SuppressWarnings("rawtypes")
public  PathService.Processor processor;
  private String configurationPath;
  private int headerLength;
  private TimeManager timeManager; 
  
  

  /**
 * @return the configurationPath
 */
public String getConfigurationPath() {
	return configurationPath;
}

/**
 * @param configurationPath the configurationPath to set
 */
public void setConfigurationPath(String configurationPath) {
	this.configurationPath = configurationPath;
}

/**
 * @return the headerLength
 */
public int getHeaderLength() {
	return headerLength;
}

/**
 * @param headerLength the headerLength to set
 */
public void setHeaderLength(int headerLength) {
	this.headerLength = headerLength;
}

public void setTimeManager(TimeManager time){
	timeManager = time;
}
public void run(){
	    try {
	    	Network network = new Network(headerLength, configurationPath);
	    	ReachabilityTree reachabilityTree = new ReachabilityTree(network.getNetworkGraph(),network.getInverseNetworkGraph(),
	    			network.getNetworkRules(), network.getInRuleList(), network.getBoxes(), network.getHeaderLength());
	        handler = new PathServiceImpl();
	        handler.setReachabilityTreeGenerator(reachabilityTree);
			handler.setTimeManager(timeManager);
	        processor = new PathService.Processor(handler);
	            simple(processor);    
	      } catch (Exception x) {
	        x.printStackTrace();
	        
	      } 
  }

  public void simple(PathService.Processor processor) {
    try {
      TServerTransport serverTransport = new TServerSocket(9090);
      TServer server = new TSimpleServer(new Args(serverTransport).processor(processor));
      //System.out.println("Starting the pathserver...");
      server.serve();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
