package searchservice;

import java.util.List;
import java.util.ArrayList;
import java.lang.*;

import org.apache.thrift.TException;

import networkmodel.*;
import searchservice.PathService.Iface;
import searchservice.TimeManager;

public class PathServiceImpl implements Iface {
	private ReachabilityTree generator;
	private TimeManager appendClassObject;

	public void setReachabilityTreeGenerator(ReachabilityTree generator) {
		this.generator = generator;
	}
	
	public void setTimeManager(TimeManager tm){
		appendClassObject = tm;
	}

	@Override
	public List<String> findPaths(int egressPort, String packetheader) throws TException {
		long startTimeNano = System.nanoTime( );
		//System.out.println("received a request, processing.... egressport is "+egressPort);
		PathWrapper wrapper = this.generator.findReversePath(egressPort, packetheader);
		long taskTimeNano  = System.nanoTime( ) - startTimeNano;
		//System.out.println("Sending the result, the result is " + wrapper.pathAndPortStrings().toString());
		appendClassObject.append(taskTimeNano);
		return wrapper.pathAndPortStrings();

	}

}

