package fuzz;

import java.util.List;
import java.util.ArrayList;
import java.lang.*;

import org.apache.thrift.TException;

import networkmodel.*;
import searchservice.FuzzService.Iface;
import searchservice.TimeManager;
import java.util.Map;
import java.util.Stack;

import jdd.bdd.BDD;

//import searchservice.PathService.Iface;
//import searchservice.TimeManager;

public class FuzzServiceImpl{
	private ReachabilityTree generator;
	private TimeManager appendClassObject;

	public void setReachabilityTreeGenerator(ReachabilityTree generator) {
		this.generator = generator;
	}

	public void setTimeManager(TimeManager tm){
		appendClassObject = tm;
	}

	@Override
	public List<String> receivepackets(BDD bdd, int id) throws TException {
		long startTimeNano = System.nanoTime( );
		System.out.println("received a packet, processing.... BDD is "+bdd);
		String wildcard = convertBDDToString(bdd, id);
		long taskTimeNano  = System.nanoTime( ) - startTimeNano;
		System.out.println("the result is " +wildcard);
		appendClassObject.append(taskTimeNano);
		return wildcard;
	}
        public List<String> convertBDDToString(BDD bdd, int id) throws TException {
                bdd.printSet(bdd);
                return bdd.printCubes(bdd);
        }
        public List<String> sendpackets(int egress, string wildcard) throws TException {
        }

}

